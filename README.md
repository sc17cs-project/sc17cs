# Human-Robot Interaction through Speech

- Author: Chengke Sun
- Email: sc17cs@leeds.ac.uk

## ROS package
You are at the main repository of this project. This repository stores the main robot programs.

## Deployment instructions
Go to [sc17cs/src/README.md](src/README.md)

## License
This project is licensed under the [MIT License](LICENSE)

## Testing Videos
Please visit [YouTube](https://www.youtube.com/playlist?list=PLiPZPOCQVcZbZsl8pcKnDFvf0BxyJ2FG_)

## Request further works based on this project
Please connect me by email.