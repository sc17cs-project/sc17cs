#!/usr/bin/env python
import math
import threading
from numpy import ndarray
import cv2
import rospy
import time
from scan8 import Scan
import os
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
from controllers.abase_controller import BaseController
from geometry_msgs.msg import Twist
import numpy as np
import tf


class Move:
    GOAL_COLOR = (255, 191, 0)
    EVALUATE_THRESHOLD = 4
    PARKING_EXPAND = 0.10
    CORRECT_ANGLE_THRESHOLD = 15
    TASK_ENTER = 0
    TASK_LEAVE = 1

    def __init__(self):
        self.__scan = Scan()
        self.__base = None  # type: BaseController

        self.bridge = CvBridge()
        self.vel_publisher = None  # type: rospy.Publisher

        self.parking_radius_pixels = 0
        self.goal_pixel = (0, 0)
        self.output_image = None  # type: ndarray
        self.listener = None  # type: tf.TransformListener

        self.base_mask = None  # type: ndarray
        self.rgbd_mask = None  # type: ndarray

        # For enter task
        self.entered_elevator = False
        self.restricted_start = 0
        self.back_to_waiting = False
        self.go_to_elevator = False

        # For leave task
        self.leaved_elevator = False

        self.current_task = None  # type: int

    def init(self):
        self.vel_publisher = rospy.Publisher('mobile_base_controller/cmd_vel', Twist, queue_size=10)
        self.__base = BaseController()
        self.__base.reg_callback(self._reached_callback)

        elevator_data = rospy.wait_for_message("sc17cs/scan/elevator", Image, timeout=3)  # type: Image
        elevator_rgb = self.bridge.imgmsg_to_cv2(elevator_data, "bgr8")
        self.__scan.init_from_img(elevator_rgb)

        parking_expand_pixels = int(round(self.PARKING_EXPAND / self.__scan.resolution))
        self.parking_radius_pixels = self.__scan.robot_radius_pixels + parking_expand_pixels

        self.listener = tf.TransformListener()
        rospy.loginfo("Ready to start tf listener...")
        time.sleep(2)

        rospy.Subscriber("sc17cs/scan/output", Image, self._make_output)
        rospy.Subscriber("sc17cs/scan/mask", Image, self._make_mask)

        show_thread = threading.Thread(target=self._show)
        show_thread.daemon = True
        show_thread.start()

        pose_thread = threading.Thread(target=self._update_pose)
        pose_thread.daemon = True
        pose_thread.start()

        # Wait the _update_pose method update the task
        while self.current_task is None:
            time.sleep(1)

        if self.current_task == self.TASK_LEAVE:
            self._correct_angle()

        start_thread = threading.Thread(target=self._start)
        start_thread.daemon = True
        start_thread.start()

    def _update_pose(self):
        """
        Update the position of the robot
        :return:
        """
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            (trans, quaternion) = self.listener.lookupTransform('/map', '/base_link', rospy.Time(0))
            x = round(trans[0], 2)
            y = round(trans[1], 2)
            euler = self._euler_from_quaternion(quaternion)
            yaw = round(euler[2], 2)
            robot_pose = (x, y, yaw)
            self.__scan.set_robot_pose(robot_pose)
            if self.current_task is None:
                if self._is_in_elevator():
                    rospy.loginfo("Robot is in the elevator, the task is leaving")
                    self.current_task = self.TASK_LEAVE
                else:
                    rospy.loginfo("Robot is not in the elevator, the task is entering")
                    self.current_task = self.TASK_ENTER
            rate.sleep()

    def _make_output(self, output_data):
        """
         Publish image as output to other modules
        :param output_data:
        :return:
        """
        output_image = self.bridge.imgmsg_to_cv2(output_data, "bgr8")

        # draw the parking pixel
        goal_col, goal_row = self.goal_pixel
        cv2.rectangle(output_image,
                      (goal_col - self.parking_radius_pixels, goal_row - self.parking_radius_pixels),
                      (goal_col + self.parking_radius_pixels, goal_row + self.parking_radius_pixels),
                      self.GOAL_COLOR, 1)
        cv2.circle(output_image, (goal_col, goal_row), self.__scan.robot_radius_pixels, self.GOAL_COLOR, -1)
        self.output_image = output_image

    def _make_mask(self, mask_data):
        mask_image = self.bridge.imgmsg_to_cv2(mask_data, "mono8")  # type: ndarray
        height, width = mask_image.shape[:2]
        mid = int(width / 2)
        self.base_mask = mask_image[:, 0:mid]
        self.rgbd_mask = mask_image[:, mid: mid * 2]

    def _show(self):
        while not rospy.is_shutdown():
            if self.output_image is None:
                time.sleep(1)
            else:
                cv2.namedWindow("scan", cv2.WINDOW_NORMAL)
                cv2.imshow("scan", self.output_image)
                cv2.waitKey(50)

    def _start(self):
        rate = rospy.Rate(3)
        while not rospy.is_shutdown():
            try:
                rospy.loginfo("Current pose (%.2f,%.2f,%.2f)", *self.__scan.robot_pose)
                rospy.loginfo("Current pixel (%d,%d)", *self.__scan.robot_pixel)
                if self.base_mask is not None and self.rgbd_mask is not None:
                    if self.current_task == self.TASK_ENTER:
                        if self.entered_elevator is True:
                            rospy.loginfo("Successfully entered the elevator, turn to leave angle")
                            self.current_task = self.TASK_LEAVE
                            self._parked()
                            break
                        self._enter()
                    else:
                        if self.leaved_elevator is True:
                            rospy.loginfo("Successfully leaved the elevator!")
                            self._parked()
                            break
                        self._leave()
                rate.sleep()
            except rospy.ROSException as e:
                rospy.logerr(e.message)
                shutdown()

    def _leave(self):
        if self._is_in_elevator() or self._is_in_restricted():
            rospy.loginfo("Robot in elevator or restricted  area")
            rospy.loginfo("Try to find leave pose")
            parking_pixel = self._generate_parking(self.__scan.elevator_and_restricted_and_waiting_gray, self.rgbd_mask,
                                                   self._evaluate_leave_elevator)
            if parking_pixel is not None:
                rospy.loginfo("Found leave pose, go to there")
                self._go_to_pixel(parking_pixel)
                return
            else:
                rospy.loginfo("No leave pose again, rotating for better view")
                self._turn_left()
                parking_pixel = self._generate_parking(self.__scan.elevator_and_restricted_and_waiting_gray,
                                                       self.rgbd_mask,
                                                       self._evaluate_leave_elevator)
                if parking_pixel is not None:
                    rospy.loginfo("Found leave pose in left, go to there")
                    self.go_to_elevator = True
                    self._go_to_pixel(parking_pixel)
                    return

                self._turn_right()
                parking_pixel = self._generate_parking(self.__scan.elevator_and_restricted_and_waiting_gray,
                                                       self.rgbd_mask,
                                                       self._evaluate_leave_elevator)
                if parking_pixel is not None:
                    rospy.loginfo("Found leave pose in right, go to there")
                    self.go_to_elevator = True
                    self._go_to_pixel(parking_pixel)
                    return

                rospy.loginfo("No leave pose and finish rotating, waiting...")
                self._correct_angle()
                return

        if self._is_in_waiting():
            self.leaved_elevator = True
            return

    def _enter(self):

        if self.back_to_waiting is True:
            rospy.loginfo("Robot is going to waiting pose...")
            parking_pixel = self._generate_parking(self.__scan.waiting_gray, self.rgbd_mask,
                                                   self._evaluate_enter_waiting)
            if parking_pixel is not None:
                rospy.loginfo("Found enter waiting pose, go to there")
                self._go_to_pixel(parking_pixel)
                return
            else:
                rospy.loginfo("No enter waiting pose, continue...")
                return

        if self._is_in_restricted():
            rospy.loginfo("Robot in restricted area.")
            if self.go_to_elevator is True:
                if self.restricted_start == 0:
                    self.restricted_start = int(time.time())
                if int(time.time()) - self.restricted_start > 10:
                    rospy.loginfo(
                        "Robot stays in restricted area for too long time, back to default waiting area.")
                    self.back_to_waiting = True
                    self.go_to_elevator = False
                    self._go_to_pixel(self.__scan.default_waiting_pixel)
                    return

                rospy.loginfo("Robot still can stay for some seconds")

                rospy.loginfo("Try to find enter pose in restricted and elevator area")
                parking_pixel = self._generate_parking(self.__scan.restricted_and_elevator_gray, self.rgbd_mask)
                if parking_pixel is None:
                    rospy.loginfo("No enter pose in restricted and elevator area, parked here")
                    self._parked()
                    rospy.loginfo("Rotating for better view")

                    self._turn_left()
                    parking_pixel = self._generate_parking(self.__scan.elevator_gray, self.rgbd_mask)
                    if parking_pixel is not None:
                        rospy.loginfo(
                            "Found enter elevator pose of restricted and elevator area in left, go to there")
                        self.restricted_start = int(time.time())
                        self._go_to_pixel(parking_pixel)
                        return

                    self._turn_right()
                    parking_pixel = self._generate_parking(self.__scan.elevator_gray, self.rgbd_mask)
                    if parking_pixel is not None:
                        rospy.loginfo(
                            "Found enter elevator pose of restricted and elevator area in right, go to there")
                        self.restricted_start = int(time.time())
                        self._go_to_pixel(parking_pixel)
                        return

                    rospy.loginfo("No enter pose in restricted and elevator area and finish rotating, waiting...")
                    return
                else:
                    rospy.loginfo("New pose in restricted and elevator area, go to there")
                    self.restricted_start = int(time.time())
                    self._go_to_pixel(parking_pixel)
                    return
            else:
                rospy.loginfo("Try to find enter waiting pose by using rgbd")
                parking_pixel = self._generate_parking(self.__scan.waiting_gray, self.rgbd_mask,
                                                       self._evaluate_enter_waiting)
                if parking_pixel is not None:
                    rospy.loginfo("Found enter waiting pose, go to there")
                    self._go_to_pixel(parking_pixel)
                    return
                else:
                    rospy.loginfo("No enter waiting pose for rgbd, try to use laser")
                    parking_pixel = self._generate_parking(self.__scan.waiting_gray, self.base_mask,
                                                           self._evaluate_enter_waiting)
                    if parking_pixel is not None:
                        rospy.loginfo("Found enter waiting pose for laser, go to there")
                        self._go_to_pixel(parking_pixel)
                        return
                    rospy.loginfo("No enter waiting pose, just parked here")
                    self._parked()
                    return

        if self._is_in_waiting():
            rospy.loginfo("Robot in waiting area")
            rospy.loginfo("Try to find enter waiting pose")
            parking_pixel = self._generate_parking(self.__scan.waiting_gray, self.rgbd_mask,
                                                   self._evaluate_enter_waiting)
            if parking_pixel is not None:
                rospy.loginfo("Found enter waiting pose, go to there")
                self._go_to_pixel(parking_pixel)
                return
            else:
                rospy.loginfo("No enter waiting pose, try to find enter elevator pose")
                parking_pixel = self._generate_parking(self.__scan.elevator_gray, self.rgbd_mask)
                if parking_pixel is not None:
                    rospy.loginfo("Found enter elevator pose, go to there")
                    self.go_to_elevator = True
                    self._go_to_pixel(parking_pixel)
                    return
                else:
                    rospy.loginfo("No enter elevator pose, is the door open?")
                    parking_pixel = self._generate_parking(self.__scan.elevator_gray, self.base_mask)
                    if parking_pixel is None:
                        rospy.loginfo("The door is closed, waiting...")
                        self._correct_angle()
                        return
                    else:
                        rospy.loginfo("The base_laser think the door is open, parked here!")
                        self._parked()
                        parking_pixel = self._generate_parking(self.__scan.elevator_gray, self.rgbd_mask)
                        if parking_pixel is not None:
                            rospy.loginfo("Found enter elevator pose, go to there")
                            self.go_to_elevator = True
                            self._go_to_pixel(parking_pixel)
                            return

                        rospy.loginfo("No enter elevator pose again, rotating for better view")
                        self._turn_left()
                        parking_pixel = self._generate_parking(self.__scan.elevator_gray, self.rgbd_mask)
                        if parking_pixel is not None:
                            rospy.loginfo("Found enter elevator pose in left, go to there")
                            self.go_to_elevator = True
                            self._go_to_pixel(parking_pixel)
                            return

                        self._turn_right()
                        parking_pixel = self._generate_parking(self.__scan.elevator_gray, self.rgbd_mask)
                        if parking_pixel is not None:
                            rospy.loginfo("Found enter elevator pose in right, go to there")
                            self.go_to_elevator = True
                            self._go_to_pixel(parking_pixel)
                            return

                        rospy.loginfo("No enter elevator pose and finish rotating, waiting...")
                        self._correct_angle()
                        return

        if self._is_in_elevator():
            rospy.loginfo("Robot in elevator area")
            rospy.loginfo("Try to find enter elevator pose")
            parking_pixel = self._generate_parking(self.__scan.elevator_gray, self.rgbd_mask)
            if parking_pixel is not None:
                rospy.loginfo("Found enter elevator pose, go to there")
                self._go_to_pixel(parking_pixel)
                return
            else:
                self.go_to_elevator = False
                rospy.loginfo("No enter elevator pose, parked here")
                self._parked()
                self.entered_elevator = True
                return

        rospy.loginfo("Robot in unknown area")
        rospy.loginfo("Try to find enter waiting pose by using rgbd")
        parking_pixel = self._generate_parking(self.__scan.waiting_gray, self.rgbd_mask, self._evaluate_enter_waiting)
        if parking_pixel is not None:
            rospy.loginfo("Found enter waiting pose, go to there")
            self._go_to_pixel(parking_pixel)
            return
        else:
            rospy.loginfo("No enter waiting pose for rgbd, try to use laser")
            parking_pixel = self._generate_parking(self.__scan.waiting_gray, self.base_mask,
                                                   self._evaluate_enter_waiting)
            if parking_pixel is not None:
                rospy.loginfo("Found enter waiting pose for laser, go to there")
                self._go_to_pixel(parking_pixel)
                return
            rospy.loginfo("No enter waiting pose, go to default waiting pose")
            self._go_to_pixel(self.__scan.default_waiting_pixel)
            return

    def _generate_parking(self, area_gray, interest_area_mask, sort_function=None):
        if sort_function is None:
            sort_function = self._evaluate_enter_elevator
        robot_col, robot_row = self.__scan.robot_pixel
        area_mask = interest_area_mask.copy()
        area_mask[
        (robot_row - self.parking_radius_pixels): (robot_row + self.parking_radius_pixels + 1),
        (robot_col - self.parking_radius_pixels):(robot_col + self.parking_radius_pixels + 1)
        ] = 255

        free_gray = cv2.bitwise_and(area_gray, area_gray, mask=area_mask)
        free_pixels = self.__scan.search_pixels(free_gray != 0)
        if len(free_pixels) == 0:
            return None

        if self.current_task == self.TASK_ENTER:
            free_pixels.sort(key=sort_function)
        else:
            free_pixels.sort(key=sort_function, reverse=True)

        current_score = sort_function(self.__scan.robot_pixel)

        for start_col, start_row in free_pixels:
            top = start_row - self.parking_radius_pixels
            bottom = start_row + self.parking_radius_pixels + 1
            left = start_col - self.parking_radius_pixels
            right = start_col + self.parking_radius_pixels + 1
            window = free_gray[top: bottom, left:right]
            if len(np.where(window == 0)[0]) == 0:
                if left <= robot_col <= right and top <= robot_row <= bottom:
                    return None
                target_score = sort_function((start_col, start_row))
                score_diff = abs(target_score - current_score)
                rospy.loginfo("Score diff: %.4f", score_diff)
                if score_diff < self.EVALUATE_THRESHOLD:
                    return None
                return start_col, start_row

        return None

    def _reached_callback(self, state):
        pass

    def _evaluate_enter_elevator(self, pixel):
        x, y = pixel

        line_a, line_b, line_c = self.__scan.elevator_bottom_line
        bottom_distance = abs(line_a * x + line_b * y + line_c) / np.sqrt(line_a ** 2 + line_b ** 2)

        line_a, line_b, line_c = self.__scan.elevator_mid_line
        mid_distance = abs(line_a * x + line_b * y + line_c) / np.sqrt(line_a ** 2 + line_b ** 2)
        return 0.7 * bottom_distance + 0.3 * mid_distance

    def _evaluate_leave_elevator(self, pixel):
        x, y = pixel

        line_a, line_b, line_c = self.__scan.elevator_bottom_line
        bottom_distance = abs(line_a * x + line_b * y + line_c) / np.sqrt(line_a ** 2 + line_b ** 2)

        return bottom_distance

    def _evaluate_enter_waiting(self, pixel):
        x, y = pixel

        line_a, line_b, line_c = self.__scan.elevator_bottom_line
        bottom_distance = abs(line_a * x + line_b * y + line_c) / np.sqrt(line_a ** 2 + line_b ** 2)

        line_a, line_b, line_c = self.__scan.elevator_mid_line
        mid_distance = abs(line_a * x + line_b * y + line_c) / np.sqrt(line_a ** 2 + line_b ** 2)
        return 0.3 * bottom_distance + 0.7 * mid_distance

    def _is_in_waiting(self):
        col, row = self.__scan.robot_pixel
        window = self.__scan.waiting_gray[
                 (row - self.parking_radius_pixels):(row + self.parking_radius_pixels + 1),
                 (col - self.parking_radius_pixels):(col + self.parking_radius_pixels + 1)
                 ]
        if self.current_task == self.TASK_ENTER:
            return len(np.where(window != 0)[0]) > 0
        else:
            return len(np.where(window == 0)[0]) == 0

    def _is_in_restricted(self):
        col, row = self.__scan.robot_pixel
        window = self.__scan.restricted_logical[
                 (row - self.parking_radius_pixels):(row + self.parking_radius_pixels + 1),
                 (col - self.parking_radius_pixels):(col + self.parking_radius_pixels + 1)
                 ]

        if self.current_task == self.TASK_ENTER:
            if self.go_to_elevator:
                return len(np.where(np.equal(window, True))[0]) > 0
        return len(np.where(np.equal(window, False))[0]) == 0

    def _is_in_elevator(self):
        col, row = self.__scan.robot_pixel
        window = self.__scan.elevator_gray[
                 (row - self.parking_radius_pixels):(row + self.parking_radius_pixels + 1),
                 (col - self.parking_radius_pixels):(col + self.parking_radius_pixels + 1)
                 ]

        return len(np.where(window == 0)[0]) == 0

    @staticmethod
    def _euler_from_quaternion(quaternion):
        # noinspection PyUnresolvedReferences
        return tf.transformations.euler_from_quaternion(quaternion)  # type: list

    def _turn_right(self, speed=-15):
        self._parked()
        angular_speed = math.radians(speed)
        relative_angle = math.radians(30)
        t0 = rospy.Time.now().to_sec()
        current_angle = 0

        desired_velocity = Twist()
        desired_velocity.linear.x = 0
        desired_velocity.linear.y = 0
        desired_velocity.linear.z = 0
        desired_velocity.angular.x = 0
        desired_velocity.angular.y = 0
        desired_velocity.angular.z = angular_speed

        while current_angle < relative_angle:
            self.vel_publisher.publish(desired_velocity)
            t1 = rospy.Time.now().to_sec()
            current_angle = abs(angular_speed) * (t1 - t0)

    def _turn_left(self):
        self._turn_right(15)

    def _go_to_pixel(self, pixel, force=False):
        if force is False and self.goal_pixel == pixel and self.__base.is_processing():
            return
        self.goal_pixel = pixel
        pose = self.__scan.pixel_to_coordinate(pixel)
        degree = self.__scan.enter_degree if self.current_task == self.TASK_ENTER else self.__scan.leave_degree
        self.__base.goto(pose, degrees=degree)

    def _reach_to_pixel(self, pixel):
        self.goal_pixel = pixel
        pose = self.__scan.pixel_to_coordinate(pixel)
        degree = self.__scan.enter_degree if self.current_task == self.TASK_ENTER else self.__scan.leave_degree
        result = self.__base.reach_to(pose, degrees=degree)
        return result

    def _parked(self):
        self.__base.cancel()
        self._reach_to_pixel(self.__scan.robot_pixel)

    def _correct_angle(self):
        current_degree = math.degrees(self.__scan.robot_pose[2])
        degree = self.__scan.enter_degree if self.current_task == self.TASK_ENTER else self.__scan.leave_degree
        diff = abs(current_degree - degree)
        if diff > self.CORRECT_ANGLE_THRESHOLD:
            rospy.loginfo("Angle diff: %.4f, correct angle...", diff)
            self._parked()


def shutdown():
    rospy.logwarn("Shutdown...")
    cv2.destroyAllWindows()
    # noinspection PyProtectedMember
    os._exit(0)


def main():
    rospy.on_shutdown(shutdown)
    rospy.init_node('sc17cs_enter')

    __move = Move()
    __move.init()

    # response the ctrl+c
    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()
