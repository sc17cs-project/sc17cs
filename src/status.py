from enum import Enum, unique


@unique
class Status(Enum):
    WAITING_GOAL = 0
    ENTERING_ELEVATOR = 1
    WAITING_ELEVATOR = 2
    WAITING_BUTTON = 3
    WAITING_LEAVE_WITHOUT_GOAL = 4
    WAITING_LEAVE = 5
    LEAVING_ELEVATOR = 6
    FINISHED = 7
