#!/usr/bin/env python

import math
import time
from numpy import ndarray
import cv2
import argparse
import numpy as np
import rospy
from geometry_msgs.msg import Point
import tf
import os
import threading
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import OccupancyGrid
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

__DIR__ = os.path.split(os.path.realpath(__file__))[0]


class Scan:
    ELEVATOR_FREE_COLOR = (0, 255, 0)
    ELEVATOR_RESTRICTED_COLOR = (0, 0, 255)
    ELEVATOR_BOTTOM_COLOR = (255, 0, 0)
    WAIT_AREA_COLOR = (0, 255, 255)

    FREE_COLOR = (0, 200, 255)
    DEFAULT_PARKING_COLOR = (117, 156, 242)
    RANGE_COLOR = (170, 255, 204)
    DETECTED_COLOR = (0, 0, 255)
    ROBOT_COLOR = (34, 102, 0)

    def __init__(self):
        self.resolution = 0.0
        self.origin = None  # type: Point
        self.map_height = 0
        self.map_width = 0
        self.map_rgb = None  # type: ndarray

        self.elevator_rgb = None  # type: ndarray
        self.elevator_gray = None  # type: ndarray
        self.waiting_gray = None  # type: ndarray
        self.waiting_and_restricted_gray = None  # type: ndarray
        self.restricted_and_elevator_gray = None  # type: ndarray
        self.elevator_and_restricted_and_waiting_gray = None  # type: ndarray
        self.restricted_logical = None  # type: ndarray
        self.interest_logical = None  # type: ndarray
        self.elevator_bottom_line = None  # type: tuple
        self.elevator_mid_line = None  # type: tuple
        self.elevator_bottom_vector = None  # type: ndarray

        self.default_elevator_pixel = (0, 0)
        self.default_waiting_pixel = (0, 0)

        self.robot_radius = 0.27
        self.robot_radius_pixels = None  # type: int

        self.listener = None  # type: tf.TransformListener
        self.robot_pose = (0.0, 0.0, 0)
        self.robot_pixel = (0, 0)
        self.enter_degree = 0.0
        self.leave_degree = 0.0

        self.bridge = None  # type: CvBridge
        self.mask_publisher = None  # type: rospy.Publisher

        self.output_map_publisher = None  # type: rospy.Publisher
        self.output_elevator_publisher = None  # type: rospy.Publisher
        self.map_publisher = None  # type: rospy.Publisher
        self.elevator_publisher = None  # type: rospy.Publisher

    def init_from_path(self, elevator_image_path):
        """
        Load from image path
        :param elevator_image_path:
        :return:
        """
        self.init_map()
        self.init_mask(cv2.imread(elevator_image_path))
        self.init_elevator_lines()

    def init_from_img(self, elevator_rgb):
        """
        Load directly from the image
        :param elevator_rgb:
        :return:
        """
        self.init_map()
        self.init_mask(elevator_rgb)
        self.init_elevator_lines()

    def init_map(self):
        map_message = rospy.wait_for_message("/map", OccupancyGrid, 3)  # type: OccupancyGrid
        self.map_width = map_message.info.width
        self.map_height = map_message.info.height
        self.resolution = map_message.info.resolution
        self.origin = map_message.info.origin.position
        map_data = np.reshape(map_message.data, (self.map_width, self.map_height))
        map_rgb = np.zeros((self.map_height, self.map_width, 3), np.uint8)
        map_rgb.fill(205)
        for row in xrange(self.map_height):
            for col in xrange(self.map_width):
                probability = map_data[row, col]
                if probability == -1:
                    continue
                if probability > 0:
                    color = 0
                else:
                    color = 255
                # color = (1 - probability) * 255
                map_rgb[row, col] = (color, color, color)
        self.map_rgb = cv2.flip(map_rgb, 0)
        self.robot_radius_pixels = int(round(self.robot_radius / self.resolution))
        rospy.loginfo("Get the map successfully, (%dx%d)", self.map_width, self.map_height)

    def init_mask(self, elevator_rgb):
        self.elevator_rgb = elevator_rgb
        # replace all 254 to 255
        self.elevator_rgb[np.where((self.elevator_rgb >= (254, 254, 254)).all(axis=2))] = (255, 255, 255)

        elevator_mask = cv2.inRange(self.elevator_rgb, self.ELEVATOR_FREE_COLOR, self.ELEVATOR_FREE_COLOR)
        elevator_bottom_mask = cv2.inRange(self.elevator_rgb, self.ELEVATOR_FREE_COLOR, self.ELEVATOR_BOTTOM_COLOR)
        elevator_mask = cv2.bitwise_or(elevator_mask, elevator_bottom_mask)
        self.elevator_gray = cv2.cvtColor(
            cv2.bitwise_and(self.elevator_rgb, self.elevator_rgb, mask=elevator_mask), cv2.COLOR_BGR2GRAY)

        _, contours, _ = cv2.findContours(elevator_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        max_contour = max(contours, key=cv2.contourArea)
        moment = cv2.moments(max_contour)
        self.default_elevator_pixel = (
            int(round(moment['m10'] / moment['m00'])),
            int(round(moment['m01'] / moment['m00']))
        )
        rospy.loginfo("Elevator free center pixel (%d,%d)", *self.default_elevator_pixel)

        waiting_mask = cv2.inRange(self.elevator_rgb, self.WAIT_AREA_COLOR, self.WAIT_AREA_COLOR)
        self.waiting_gray = cv2.cvtColor(
            cv2.bitwise_and(self.elevator_rgb, self.elevator_rgb, mask=waiting_mask), cv2.COLOR_BGR2GRAY)
        _, contours, _ = cv2.findContours(waiting_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        max_contour = max(contours, key=cv2.contourArea)
        moment = cv2.moments(max_contour)
        self.default_waiting_pixel = (
            int(round(moment['m10'] / moment['m00'])),
            int(round(moment['m01'] / moment['m00']))
        )
        rospy.loginfo("Default waiting pixel (%d,%d)", *self.default_waiting_pixel)

        restricted_mask = cv2.inRange(self.elevator_rgb, self.ELEVATOR_RESTRICTED_COLOR,
                                      self.ELEVATOR_RESTRICTED_COLOR)
        restricted_and_elevator_mask = cv2.bitwise_or(restricted_mask, elevator_mask)
        self.restricted_and_elevator_gray = cv2.cvtColor(
            cv2.bitwise_and(self.elevator_rgb, self.elevator_rgb, mask=restricted_and_elevator_mask),
            cv2.COLOR_BGR2GRAY)

        waiting_and_restricted_mask = cv2.bitwise_or(waiting_mask, restricted_mask)
        self.waiting_and_restricted_gray = cv2.cvtColor(
            cv2.bitwise_and(self.elevator_rgb, self.elevator_rgb, mask=waiting_and_restricted_mask),
            cv2.COLOR_BGR2GRAY)

        elevator_and_restricted_and_waiting_mask = cv2.bitwise_or(elevator_mask, restricted_mask)
        elevator_and_restricted_and_waiting_mask = cv2.bitwise_or(elevator_and_restricted_and_waiting_mask,
                                                                  waiting_mask)
        self.elevator_and_restricted_and_waiting_gray = cv2.cvtColor(
            cv2.bitwise_and(self.elevator_rgb, self.elevator_rgb, mask=elevator_and_restricted_and_waiting_mask),
            cv2.COLOR_BGR2GRAY)

        self.restricted_logical = restricted_mask == 255
        elevator_logical = elevator_mask == 255
        wait_area_logical = (self.elevator_rgb == self.WAIT_AREA_COLOR).all(axis=2)
        self.interest_logical = np.logical_or(self.restricted_logical,
                                              np.logical_or(elevator_logical, wait_area_logical))

    def init_elevator_lines(self):
        """
        Calculate the bottom and center lines of the elevator area
        :return:
        """
        bottom_pixels = np.array(self.search_pixels((self.elevator_rgb == self.ELEVATOR_BOTTOM_COLOR).all(axis=2)))
        self.elevator_bottom_vector = np.mean(bottom_pixels, axis=0).reshape((2, 1))
        elevator_bottom_pixel = tuple([int(round(n)) for n in np.mean(np.array(bottom_pixels), axis=0).tolist()])

        # Fit the inner boundary into a straight line
        vx, vy, cx, cy = cv2.fitLine(bottom_pixels, cv2.DIST_L2, 0, 0.01, 0.01)

        # Should big enough
        m = 200

        # line_start/line_end = (x,y) => (col,row)
        line_start = (float(cx - m * vx[0]), float(cy - m * vy[0]))
        line_end = (float(cx + m * vx[0]), float(cy + m * vy[0]))

        # Fitting line [ line_a*x+ line_b*y + line_c = 0 ]
        line_a = line_end[1] - line_start[1]
        line_b = -1 * (line_end[0] - line_start[0]) + 1e-8
        line_c = -1 * line_a * elevator_bottom_pixel[0] - line_b * elevator_bottom_pixel[1]
        self.elevator_bottom_line = (line_a, line_b, line_c)
        line_slope = -1 * line_a / line_b
        line_slope_degree = math.degrees(math.atan(line_slope))
        rospy.loginfo("Bottom line: %.4f*x + %.4f*y + %.4f = 0; Slope: %.4f, Slope degree: %.4f", line_a, line_b,
                      line_c, line_slope, line_slope_degree)

        mid_line_a = line_b
        mid_line_b = line_a + 1e-8
        mid_line_c = -1 * mid_line_a * elevator_bottom_pixel[0] - mid_line_b * elevator_bottom_pixel[1]
        self.elevator_mid_line = (mid_line_a, mid_line_b, mid_line_c)
        rospy.loginfo("Mid line: %.4f*x + %.4f*y + %.4f = 0", mid_line_a, mid_line_b, mid_line_c)

        if line_slope_degree == 0:
            enter_degree = 90.0
        elif line_slope_degree < 0:
            if self.default_elevator_pixel[1] < elevator_bottom_pixel[1]:
                enter_degree = -1 * (90 + line_slope_degree)
            else:
                enter_degree = 90 - line_slope_degree
        else:
            if self.default_elevator_pixel[1] < elevator_bottom_pixel[1]:
                enter_degree = -1 * (90 + line_slope_degree)
            else:
                enter_degree = 90 - line_slope_degree
        self.enter_degree = round(enter_degree, 2)
        if enter_degree >= 0:
            self.leave_degree = self.enter_degree - 180
        else:
            self.leave_degree = self.enter_degree + 180
        rospy.loginfo("Enter degree %.2f. Leave degree %.2f", self.enter_degree, self.leave_degree)

    def start(self):
        self.bridge = CvBridge()
        self.map_publisher = rospy.Publisher('sc17cs/scan/map', Image, queue_size=5, latch=True)
        self.elevator_publisher = rospy.Publisher('sc17cs/scan/elevator', Image, queue_size=5, latch=True)
        self.mask_publisher = rospy.Publisher('sc17cs/scan/mask', Image, queue_size=5)
        self.output_map_publisher = rospy.Publisher('sc17cs/scan/output/map', Image, queue_size=5)
        self.output_elevator_publisher = rospy.Publisher('sc17cs/scan/output/elevator', Image, queue_size=5)

        self.map_publisher.publish(self.bridge.cv2_to_imgmsg(self.map_rgb, "bgr8"))
        self.elevator_publisher.publish(self.bridge.cv2_to_imgmsg(self.elevator_rgb, "bgr8"))

        self.listener = tf.TransformListener()
        rospy.loginfo("Ready to start tf listener...")
        time.sleep(3)

        update_thread = threading.Thread(target=self.update)
        update_thread.daemon = True
        update_thread.start()

    def set_robot_pose(self, pose):
        """
        Set the robot pose
        :param pose:
        :return:
        """
        self.robot_pose = pose
        self.robot_pixel = self.coordinate_to_pixel(self.robot_pose)

    def update(self):
        while True:
            try:
                start_time = time.time()

                (trans, quaternion) = self.listener.lookupTransform('/map', '/base_link', rospy.Time(0))
                x = round(trans[0], 2)
                y = round(trans[1], 2)
                euler = self.euler_from_quaternion(quaternion)
                yaw = round(euler[2], 2)
                self.robot_pose = (x, y, yaw)
                self.robot_pixel = self.coordinate_to_pixel(self.robot_pose)
                output_image = self.map_rgb.copy()
                elevator_image = self.elevator_rgb.copy()

                base_mask = self.generate_laser_mask()
                rgbd_mask = self.generate_rgbd_mask(base_mask, output_image)

                # draw the pixel of robot
                cv2.circle(output_image, self.robot_pixel, self.robot_radius_pixels, self.ROBOT_COLOR, -1)
                cv2.circle(elevator_image, self.robot_pixel, self.robot_radius_pixels, self.ROBOT_COLOR, -1)

                merged_mask = cv2.hconcat((base_mask, rgbd_mask))

                self.mask_publisher.publish(self.bridge.cv2_to_imgmsg(merged_mask, "mono8"))
                self.output_map_publisher.publish(self.bridge.cv2_to_imgmsg(output_image, "bgr8"))
                self.output_elevator_publisher.publish(self.bridge.cv2_to_imgmsg(elevator_image, "bgr8"))

                end_time = time.time()
                rospy.loginfo("%.4fs. Publish new mask", end_time - start_time)
            except rospy.ROSException as e:
                rospy.logerr(e.message)
                shutdown()

    def pixel_to_coordinate(self, col, row=None):
        """
        :type col: list|Tuple|int
        :type row: None|int
        """
        if hasattr(col, '__len__') and len(col) >= 2 and row is None:
            row = col[1]
            col = col[0]
        result = [round(col * self.resolution + self.origin.x, 2),
                  round((self.map_height - row) * self.resolution + self.origin.y, 2)]
        return tuple(result)

    def coordinate_to_pixel(self, x, y=None):
        """
        :type x: list|Tuple|float
        :type y: None|float
        """
        if hasattr(x, '__len__') and len(x) >= 2 and y is None:
            y = x[1]
            x = x[0]
        return int(round((x - self.origin.x) / self.resolution)), self.map_height - int(
            round(((y - self.origin.y) / self.resolution)))

    @staticmethod
    def euler_from_quaternion(quaternion):
        # noinspection PyUnresolvedReferences
        return tf.transformations.euler_from_quaternion(quaternion)  # type: list

    def generate_laser_mask(self):
        (trans_laser, quaternion_laser) = self.listener.lookupTransform("/map", "/base_laser_link", rospy.Time(0))
        laser_message = rospy.wait_for_message("/scan", LaserScan, 1)  # type: LaserScan

        x = trans_laser[0]
        y = trans_laser[1]
        euler = self.euler_from_quaternion(quaternion_laser)
        yaw = euler[2]
        laser_pose = (x, y, yaw)
        laser_pixel = self.coordinate_to_pixel(laser_pose)

        line_pixels = [laser_pixel]
        outside_distance = -0.5
        relative_x = np.cos(laser_pose[2]) * outside_distance
        relative_y = np.sin(laser_pose[2]) * outside_distance

        x = trans_laser[0] + relative_x
        y = trans_laser[1] + relative_y
        outside_pixel = self.coordinate_to_pixel(x, y)

        distances = laser_message.ranges
        range_min = laser_message.range_min
        range_max = laser_message.range_max
        angle_min = laser_message.angle_min
        angle_increment = laser_message.angle_increment
        previous_pixel = None  # type: tuple

        for index in xrange(len(distances)):
            distance = distances[index]
            if math.isnan(distance):
                distance = range_min
            if math.isinf(distance):
                distance = range_max
            if distance > range_max:
                distance = range_max

            angle = angle_min + angle_increment * index + laser_pose[2]

            relative_x = np.cos(angle) * distance
            relative_y = np.sin(angle) * distance

            x = trans_laser[0] + relative_x
            y = trans_laser[1] + relative_y

            pixel = self.coordinate_to_pixel(x, y)

            if pixel == previous_pixel:
                continue
            previous_pixel = pixel
            line_pixels.append(pixel)

        if len(line_pixels) > 3:
            line_mask = np.full((self.map_height, self.map_width), 0, np.uint8)
            laser_mask = np.full((self.map_height, self.map_width), 255, np.uint8)
            for i in xrange(len(line_pixels)):
                line_pixel = line_pixels[i]
                if i + 1 < len(line_pixels):
                    next_pixel = line_pixels[i + 1]
                else:
                    next_pixel = line_pixels[0]
                cv2.line(line_mask, line_pixel, next_pixel, 255, 1)

            fill_mask = np.insert(line_mask, 0, values=0, axis=0)
            fill_mask = np.insert(fill_mask, 0, values=0, axis=1)
            fill_mask = np.insert(fill_mask, len(fill_mask), values=0, axis=0)
            fill_mask = np.insert(fill_mask, len(fill_mask[0]), values=0, axis=1)

            cv2.floodFill(laser_mask, fill_mask, outside_pixel, 0, cv2.FLOODFILL_MASK_ONLY)
            laser_mask[np.logical_and(laser_mask == 255, line_mask == 255)] = 0
        else:
            laser_mask = np.full((self.map_height, self.map_width), 0, np.uint8)

        interest_mask = np.full((self.map_height, self.map_width), 0, np.uint8)
        interest_mask[np.logical_and(laser_mask == 255, self.interest_logical)] = 255

        return interest_mask

    def generate_rgbd_mask(self, base_laser_mask, output_image):
        # rgbd_queue = Queue.Queue()
        # rgbd_thread = threading.Thread(target=self.get_rgbd_scan, args=(rgbd_queue,))
        # rgbd_thread.daemon = True
        # rgbd_thread.start()
        # rgbd_thread.join()
        #
        # rgbd_data = rgbd_queue.get()
        # trans_laser = rgbd_data[0]  # type: tuple
        # laser_message = rgbd_data[1]  # type: LaserScan

        (trans_laser, quaternion_laser) = self.listener.lookupTransform("/map", "/rgbd_laser_link", rospy.Time(0))
        laser_message = rospy.wait_for_message("/rgbd_scan", LaserScan, 1)  # type: LaserScan

        x = trans_laser[0]
        y = trans_laser[1]
        euler = self.euler_from_quaternion(quaternion_laser)
        yaw = euler[2]
        laser_pose = (x, y, yaw)
        laser_pixel = self.coordinate_to_pixel(laser_pose)

        line_pixels = [laser_pixel]
        outside_distance = -0.5
        relative_x = np.cos(laser_pose[2]) * outside_distance
        relative_y = np.sin(laser_pose[2]) * outside_distance

        x = trans_laser[0] + relative_x
        y = trans_laser[1] + relative_y
        outside_pixel = self.coordinate_to_pixel(x, y)

        distances = laser_message.ranges
        range_min = laser_message.range_min
        range_max = laser_message.range_max
        angle_min = laser_message.angle_min
        angle_max = laser_message.angle_max
        angle_increment = laser_message.angle_increment
        previous_pixel = None  # type: tuple

        for index in xrange(len(distances)):
            angle = angle_min + angle_increment * index + self.robot_pose[2]
            distance = distances[index]
            if math.isnan(distance):
                distance = range_min
            if math.isinf(distance):
                distance = range_max
            if distance > range_max:
                distance = range_max
            relative_x = np.cos(angle) * distance
            relative_y = np.sin(angle) * distance
            x = trans_laser[0] + relative_x
            y = trans_laser[1] + relative_y

            pixel = self.coordinate_to_pixel(x, y)
            if pixel == previous_pixel:
                continue
            previous_pixel = pixel
            line_pixels.append(pixel)

        if len(line_pixels) > 3:
            line_mask = np.full((self.map_height, self.map_width), 0, np.uint8)
            laser_mask = np.full((self.map_height, self.map_width), 255, np.uint8)
            for i in xrange(len(line_pixels)):
                line_pixel = line_pixels[i]
                if i + 1 < len(line_pixels):
                    next_pixel = line_pixels[i + 1]
                else:
                    next_pixel = line_pixels[0]
                cv2.line(line_mask, line_pixel, next_pixel, 255, 1)

            fill_mask = np.insert(line_mask, 0, values=0, axis=0)
            fill_mask = np.insert(fill_mask, 0, values=0, axis=1)
            fill_mask = np.insert(fill_mask, len(fill_mask), values=0, axis=0)
            fill_mask = np.insert(fill_mask, len(fill_mask[0]), values=0, axis=1)

            cv2.floodFill(laser_mask, fill_mask, outside_pixel, 0, cv2.FLOODFILL_MASK_ONLY)
            laser_mask[np.logical_and(laser_mask == 255, line_mask == 255)] = 0
        else:
            laser_mask = np.full((self.map_height, self.map_width), 0, np.uint8)

        laser_mask[base_laser_mask == 0] = 0

        interest_mask = np.full((self.map_height, self.map_width), 0, np.uint8)
        interest_mask[np.logical_and(laser_mask == 255, self.interest_logical)] = 255
        output_image[interest_mask == 255] = self.FREE_COLOR

        # draw rgbd range
        rgbd_range_mask = np.zeros((self.map_height, self.map_width, 1), np.uint8)
        rgbd_angle_range = abs(angle_max - angle_min)

        radius = int(round(range_max / self.resolution))
        laser_pixel = self.coordinate_to_pixel(laser_pose)
        center = (laser_pixel[0], self.map_height - laser_pixel[1])
        axes = (radius, radius)

        angle = math.degrees(laser_pose[2] - rgbd_angle_range / 2)
        start_angle = 0
        end_angle = math.degrees(rgbd_angle_range)
        cv2.ellipse(rgbd_range_mask, center, axes, angle, start_angle, end_angle, 255, -1)
        mask_image = cv2.flip(rgbd_range_mask, 0)

        output_image[
            np.logical_and(mask_image == 255, (output_image == (255, 255, 255)).all(axis=2))] = self.RANGE_COLOR

        return interest_mask

    @staticmethod
    def search_pixels(condition):
        pixels = []
        pixels_search = np.where(condition)
        for index in xrange(len(pixels_search[0])):
            pixels.append((pixels_search[1][index], pixels_search[0][index]))
        return pixels

    @staticmethod
    def rgb_to_hsv(rgb_color):
        rgb_image = np.uint8([[rgb_color]])
        hsv_image = cv2.cvtColor(rgb_image, cv2.COLOR_BGR2HSV)
        return hsv_image[0, 0]


def shutdown():
    rospy.logwarn("Shutdown...")
    cv2.destroyAllWindows()
    # noinspection PyProtectedMember
    os._exit(0)


def main():
    ap = argparse.ArgumentParser()
    # noinspection PyTypeChecker
    ap.add_argument("--elevator", type=str, help="The path of elevator.png", default='../maps/dev/elevator.png')

    args = vars(ap.parse_args(rospy.myargv()[1:]))
    args = dict((k, v) for k, v in args.iteritems() if v is not None)

    params = {'elevator_image_path': os.path.abspath(os.path.join(__DIR__, os.path.expandvars(args["elevator"])))}
    print("params", params)

    rospy.on_shutdown(shutdown)
    rospy.init_node('sc17cs_scan')

    __scan = Scan()
    __scan.init_from_path(**params)
    __scan.start()

    # response the ctrl+c
    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()
