# Deployment instructions

Want to go back to the overall introduction of this project? click [here](../README.md)

## Install ROS

Please follow this link to install ROS (The Kinetic Version).

http://wiki.ros.org/Robots/TIAGo/Tutorials/Installation/InstallUbuntuAndROS

## Install TIAGo Simulation

First of all open a terminal and create an empty workspace: 
```
mkdir ~/tiago_public_ws
cd ~/tiago_public_ws
```
Download the file [sc17cs_project.rosinstall](https://gitlab.com/sc17cs-project/sc17cs/raw/master/sc17cs_project.rosinstall). Copy the rosinstall file in ~/tiago_public_ws. Then run the following instruction in order to clone all the required repositories within the workspace: 
```
rosinstall src /opt/ros/kinetic sc17cs_project.rosinstall
```

Visit the following link to continue to install TIAGo Simulation. You **only** need to execute the command after the `rosinstall` step. When you finished, go back and continue to the next step.

http://wiki.ros.org/Robots/TIAGo/Tutorials/Installation/TiagoSimulation

## Load workspace automatically
Append the following command to `~/.bashrc`
```
source $HOME/tiago_public_ws/devel/setup.bash
```

And run the following command.
```
source ~/.bashrc
```

## Copy Gazebo Models

```
cd ~/tiago_public_ws/src/sc17cs/
mkdir ~/.gazebo
cp -r models ~/.gazebo/
cp -r building_editor_models ~/
```

### Copy Pose Config
```
mkdir ~/.pal
cd ~/tiago_public_ws/src/sc17cs/maps
cp pose.yaml ~/.pal
```

## Installation Test

Please run the following command, and your Gazebo should load the Test World 1 successfully.
```
roslaunch sc17cs tiago_navigation_test1.launch
```

## Install Node.js
Visit the following link to install Node.js

https://nodejs.org/en/download/package-manager/

## Install Speech Service

Please run the following commands

```
mkdir ~/sc17cs_speech
cd ~/sc17cs_speech
git clone https://gitlab.com/sc17cs-project/speech_service.git
cd speech_service
npm install
```

## Install STT module

Please run the following commands (Suppose you are using Ubuntu)
```
sudo apt-get install libmagic-dev libatlas-base-dev sox libsox-fmt-all
mkdir ~/sc17cs_speech
cd ~/sc17cs_speech
git clone https://gitlab.com/sc17cs-project/stt.git
cd stt
npm install
```
More information about the STT module can be found [here](https://gitlab.com/sc17cs-project/stt/blob/master/README.md)

## Speech Service Test

Open a new terminal and run the following commands
```
cd ~/sc17cs_speech
cd speech_service
npm run demo
```

You can use the `npm run demo` command to use a demo service provided by Chengke Sun until **December 2019**. If you want to build your own speech service, you need to create your own Google Cloud Platform project. You can visit [here](https://gitlab.com/sc17cs-project/speech_service/blob/master/README.md) to get some help.


Open a new terminal and run the following commands
```
cd ~/sc17cs_speech
cd stt
npm run gazebo
```

Now, the system should respond to the hotword "Alexa" and your command. The recognition result of the command will be displayed in the second terminal.

## You can run the demos in Test 1, 2 and 3 now.
Make sure that the two terminals you open in the Speech Service Test section are still running.

### Run the demo in Test World 1
Open a new terminal and run the following command
```
roslaunch sc17cs tiago_navigation_test1.launch
```

Open a new terminal and run the following command
```
roslaunch sc17cs gazebo_main4_test1.launch
```

### Run the demo in Test World 2
Open a new terminal and run the following command
```
roslaunch sc17cs tiago_navigation_test2.launch
```

Open a new terminal and run the following command
```
roslaunch sc17cs gazebo_main4_test2.launch
```

### Run the demo in Test World 3
Open a new terminal and run the following command
```
roslaunch sc17cs tiago_navigation_dev_crowd.launch
```

Open a new terminal and run the following command
```
roslaunch sc17cs gazebo_main4_test3.launch
```

#### Open and Close the elevator doors
Open a new terminal and run the following command
```
rosrun sc17cs door.py --open
```
```
rosrun sc17cs door.py --close
```

#### Move all the dummy to the inside of the elevator
Open a new terminal and run the following command
```
rosrun sc17cs person_moving.py
```