#!/usr/bin/env python
import rospy
import socket
import argparse
import os
import std_msgs.msg
import time
import threading
import requests
from requests import RequestException
import json


class Intent:
    def __init__(self):
        self.goal_set_publisher = rospy.Publisher('sc17cs/intent/goal_set', std_msgs.msg.Int16, queue_size=5)
        self.goal_reach_publisher = rospy.Publisher('sc17cs/intent/goal_reach', std_msgs.msg.Bool, queue_size=5)
        self.button_pressed_publisher = rospy.Publisher('sc17cs/intent/button_pressed', std_msgs.msg.Bool, queue_size=5)
        self.confirm_publisher = rospy.Publisher('sc17cs/intent/confirm', std_msgs.msg.Bool, queue_size=5)
        self.repeat_publisher = rospy.Publisher('sc17cs/intent/repeat', std_msgs.msg.Bool, queue_size=5)
        self.none_publisher = rospy.Publisher('sc17cs/intent/none', std_msgs.msg.Bool, queue_size=5)

        self.client = None  # type:socket.socket

    def start(self, ip='127.0.0.1', port=1201):
        rospy.loginfo("Try to connect STT server, IP: %s, Port: %d", ip, port)
        self.client = socket.socket()
        self.client.settimeout(30)
        try:
            self.client.connect((ip, port))
        except socket.error:
            rospy.logerr("STT Connected Failed")
            shutdown()
        rospy.loginfo("STT Connected successfully")

        heart_thread = threading.Thread(target=self._heart)
        heart_thread.daemon = True
        heart_thread.start()

        while True:
            try:
                message = self.client.makefile().readline().strip("\n").strip()
                if len(message) > 0:
                    rospy.loginfo("Get message: %s", message)
                    self._pub_intent(message)
            except socket.timeout:
                pass
            time.sleep(0.5)

    def _heart(self):
        """
        Heartbeat package
        :return:
        """
        while True:
            try:
                self.client.sendall("ping".encode())
            except socket.error:
                rospy.logerr("STT server is down")
                shutdown()
            time.sleep(3)

    def _pub_intent(self, message):
        """
        Release intention
        :param message:
        :return:
        """
        headers = {
            'Ocp-Apim-Subscription-Key': '73abc6c7234d4c6fbc849a9956207417',
        }

        params = {
            # Query parameter
            'q': message,
            # Optional request parameters, set to default values
            # 'timezoneOffset': '0',
            # 'verbose': 'false',
            # 'spellCheck': 'false',
            # 'staging': 'false',
        }

        try:
            response = requests.get(
                'https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/5243d353-80cc-48af-a5d4-f703911eadb5',
                headers=headers, params=params)
            try:
                if response.status_code == 200:
                    data = response.json()
                    rospy.loginfo("LUIS Result: %s", json.dumps(data))
                    if 'topScoringIntent' in data:
                        intent = data['topScoringIntent']
                        if intent['score'] > 0.5:
                            intent_name = intent['intent']
                            if intent_name == 'Goal.Set':
                                if type(data['entities']) == list and len(data['entities']) > 0:
                                    self._pub_goal_set(data['entities'])
                                    return
                            elif intent_name == 'Goal.Reach':
                                self._pub_goal_reach()
                                return
                            elif intent_name == 'Button.Pressed':
                                self._pub_button_pressed()
                                return
                            elif intent_name == 'Utilities.Cancel':
                                self._pub_cancel()
                                return
                            elif intent_name == 'Utilities.Confirm':
                                self._pub_confirm()
                                return
                            elif intent_name == 'Utilities.Repeat':
                                self._pub_repeat()
                                return
                        self._pub_none()
                        return
                else:
                    rospy.logerr("LUIS error: %d", response.status_code)

            except ValueError as e:
                rospy.logerr(e.message)

        except RequestException as e:
            rospy.logerr(e.message)

    def _pub_goal_set(self, entities):
        if len(entities) > 0:
            entity = entities[0]
            if entity['type'] == 'builtin.number' or entity['type'] == 'builtin.ordinal':
                value = int(entity['resolution']['value'])
                rospy.loginfo("goal_set published: %d", value)
                self.goal_set_publisher.publish(value)

    def _pub_goal_reach(self):
        rospy.loginfo('goal_reach published')
        self.goal_reach_publisher.publish(True)

    def _pub_button_pressed(self):
        rospy.loginfo('button_pressed published')
        self.button_pressed_publisher.publish(True)

    def _pub_cancel(self):
        rospy.loginfo('cancel published')
        self.confirm_publisher.publish(False)

    def _pub_confirm(self):
        rospy.loginfo('confirm published')
        self.confirm_publisher.publish(True)

    def _pub_repeat(self):
        rospy.loginfo('repeat published')
        self.repeat_publisher.publish(True)

    def _pub_none(self):
        rospy.loginfo('none published')
        self.none_publisher.publish(True)


def shutdown():
    rospy.logwarn("Shutdown...")
    # noinspection PyProtectedMember
    os._exit(0)


def main():
    ap = argparse.ArgumentParser()
    # noinspection PyTypeChecker
    ap.add_argument("--ip", type=str, help="ip address", default=None)
    # noinspection PyTypeChecker
    ap.add_argument("--port", type=str, help="port", default=None)

    args = vars(ap.parse_args(rospy.myargv()[1:]))
    args = dict((k, v) for k, v in args.iteritems() if v is not None)

    rospy.on_shutdown(shutdown)
    rospy.init_node('sc17cs_intent')

    __pub = Intent()
    __pub.start(**args)


if __name__ == '__main__':
    main()
