import curses
import sys
import os


def main(win):
    """
    Control window test
    :param win:
    :return:
    """
    win.nodelay(True)
    win.clear()
    win.addstr("Detected key:")
    while True:
        while True:
            try:
                key_code = win.getch()
                if key_code == -1:
                    break
                win.clear()
                win.addstr("Detected key:")
                win.addstr(str(key_code))
            except KeyboardInterrupt:
                sys.exit(0)


if __name__ == '__main__':
    curses.wrapper(main)
