#!/usr/bin/env python
import time

import tf
from geometry_msgs.msg import Twist
import rospy
import math


def _euler_from_quaternion(quaternion):
    # noinspection PyUnresolvedReferences
    return tf.transformations.euler_from_quaternion(quaternion)  # type: list


def main():
    """
    Manually correct the robot angle
    :return:
    """
    vel_publisher = rospy.Publisher('mobile_base_controller/cmd_vel', Twist, queue_size=10)
    listener = tf.TransformListener()
    time.sleep(4)
    min_speed = math.radians(5)
    max_speed = math.radians(40)
    rate = rospy.Rate(10)
    mercy_degrees = 3
    while not rospy.is_shutdown():
        (trans, quaternion) = listener.lookupTransform('/map', '/base_link', rospy.Time(0))
        euler = _euler_from_quaternion(quaternion)
        yaw = round(euler[2], 2)
        yaw_degrees = math.degrees(yaw)
        diff_degrees = 90 - yaw_degrees
        if abs(diff_degrees) > mercy_degrees:

            diff_times = int(math.floor(diff_degrees / mercy_degrees))
            if diff_times > 2:
                angular_speed = diff_times * min_speed * 0.8
                if angular_speed > max_speed:
                    angular_speed = max_speed
            else:
                angular_speed = min_speed

            desired_velocity = Twist()
            desired_velocity.linear.x = 0
            desired_velocity.linear.y = 0
            desired_velocity.linear.z = 0
            desired_velocity.angular.x = 0
            desired_velocity.angular.y = 0
            desired_velocity.angular.z = angular_speed if diff_degrees > 0 else angular_speed * -1
            vel_publisher.publish(desired_velocity)
            rospy.loginfo("Correcting, diff: %.2f, speed: %.2f degrees/s", diff_degrees, math.degrees(angular_speed))
        else:
            rospy.loginfo("Stop, angle diff: %.2f", diff_degrees)
            return
        rate.sleep()


if __name__ == '__main__':
    rospy.init_node('sc17cs_correct_angle')
    main()
