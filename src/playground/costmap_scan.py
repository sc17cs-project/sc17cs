import numpy as np
import cv2
from numpy import ndarray
import rospy
from nav_msgs.msg import OccupancyGrid
from geometry_msgs.msg import Point
import os
import threading
import time


class CostmapScan:
    def __init__(self):
        self.output_image = None  # type: ndarray
        self.origin = None  # type: Point
        self.map_height = 0
        self.map_width = 0
        self.map_rgb = None  # type: ndarray
        self.resolution = 0.0

        rospy.on_shutdown(self.shutdown)
        rospy.init_node('sc17cs_costmap_scan', anonymous=True)

    @staticmethod
    def shutdown():
        rospy.logwarn("Shutdown...")
        cv2.destroyAllWindows()
        # noinspection PyProtectedMember
        os._exit(0)

    def start(self):
        update_thread = threading.Thread(target=self.update)
        update_thread.daemon = True
        update_thread.start()

        show_thread = threading.Thread(target=self.show)
        show_thread.daemon = True
        show_thread.start()

    def update(self):
        """
        Update costmap
        :return:
        """
        while not rospy.is_shutdown():
            map_message = rospy.wait_for_message("/move_base/global_costmap/costmap", OccupancyGrid,
                                                 3)  # type: OccupancyGrid
            self.map_width = map_message.info.width
            self.map_height = map_message.info.height
            self.resolution = map_message.info.resolution
            self.origin = map_message.info.origin.position
            map_data = np.reshape(map_message.data, (self.map_width, self.map_height))
            map_rgb = np.zeros((self.map_height, self.map_width, 3), np.uint8)
            map_rgb.fill(205)
            for row in range(self.map_height):
                for col in range(self.map_width):
                    probability = map_data[row, col]
                    if probability == -1:
                        continue
                    color = (1 - probability) * 255
                    map_rgb[row, col] = (color, color, color)
            self.map_rgb = cv2.flip(map_rgb, 0)
            self.output_image = self.map_rgb

    def show(self):
        while True:
            if self.output_image is not None:
                cv2.namedWindow("costmap", cv2.WINDOW_NORMAL)
                cv2.imshow("costmap", self.output_image)
                cv2.waitKey(50)
            else:
                time.sleep(1)
        pass


def main():
    __scan = CostmapScan()
    __scan.start()
    while True:
        time.sleep(0.1)


if __name__ == '__main__':
    main()
