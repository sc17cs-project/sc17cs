#!/usr/bin/env python
import math
import time
import cv2
from nav_msgs.msg import OccupancyGrid
import numpy as np
import rospy
from geometry_msgs.msg import Point
import tf
import os
import threading
from sensor_msgs.msg import LaserScan


class GetScanning:
    def __init__(self):
        self.resolution = 0.0
        self.robot_radius = 0.27
        self.origin = None  # type: Point
        self.map_height = 0
        self.map_width = 0
        self.map_image = None
        self.output_image = None
        self.laser_mask = None
        self.listener = None
        self.robot_pose = (0, 0)
        rospy.on_shutdown(self.shutdown)
        rospy.init_node('rgbd_range', anonymous=True)

    @staticmethod
    def shutdown():
        # noinspection PyProtectedMember
        os._exit(0)

    def load(self):
        map_message = rospy.wait_for_message("/map", OccupancyGrid, 3)  # type: OccupancyGrid
        self.map_width = map_message.info.width
        self.map_height = map_message.info.height
        self.resolution = map_message.info.resolution
        self.origin = map_message.info.origin.position
        map_data = np.reshape(map_message.data, (self.map_width, self.map_height))
        rgb_image = np.zeros((self.map_height, self.map_width, 3), np.uint8)
        rgb_image.fill(205)
        for row in range(self.map_height):
            for col in range(self.map_width):
                probability = map_data[row, col]
                if probability == -1:
                    continue
                if probability > 0:
                    color = 0
                else:
                    color = 255
                # color = (1 - probability) * 255
                rgb_image[row, col] = (color, color, color)
        self.map_image = cv2.flip(rgb_image, 0)

        self.listener = tf.TransformListener()
        print("Ready to start tf listener...")
        time.sleep(3)

        update_thread = threading.Thread(target=self.update)
        update_thread.daemon = True
        update_thread.start()

        show_thread = threading.Thread(target=self.show)
        show_thread.daemon = True
        show_thread.start()

    def pixel_to_coordinate(self, row, col):
        return [col * self.resolution + self.origin.x, (self.map_height - row) * self.resolution + self.origin.y]

    def coordinate_to_pixel(self, x, y):
        return [self.map_height - int(round(((y - self.origin.y) / self.resolution))),
                int(round((x - self.origin.x) / self.resolution))]

    @staticmethod
    def s(t):
        return t[0] + 0, t[1] + 0

    @staticmethod
    def euler_from_quaternion(quaternion):
        # noinspection PyUnresolvedReferences
        return tf.transformations.euler_from_quaternion(quaternion)  # type: list

    def update(self):
        while True:
            (trans, quaternion) = self.listener.lookupTransform('/map', '/base_link', rospy.Time(0))
            x = trans[0]
            y = trans[1]
            euler = self.euler_from_quaternion(quaternion)
            yaw = euler[2]
            self.robot_pose = (x, y, yaw)

            free_pixel = None

            output_image = self.map_image.copy()
            robot_pixel = self.coordinate_to_pixel(self.robot_pose[0], self.robot_pose[1])

            (trans_laser, quaternion_laser) = self.listener.lookupTransform('/map', '/rgbd_laser_link', rospy.Time(0))
            yaw = euler[2]
            x = trans_laser[0]
            y = trans_laser[1]
            laser_pose = (x, y, yaw)
            laser_pixel = self.coordinate_to_pixel(laser_pose[0], laser_pose[1])
            laser_pixel.reverse()
            laser_pixel = tuple(laser_pixel)

            # Get data from RGBD camera
            laser_message = rospy.wait_for_message("/rgbd_scan", LaserScan, 1)  # type: LaserScan
            distances = laser_message.ranges
            angle_min = laser_message.angle_min
            angle_increment = laser_message.angle_increment
            previous_pixel = None  # type: tuple

            line_pixels = [laser_pixel]
            pixel = None

            free_d = -0.5
            relative_x = np.cos(laser_pose[2]) * free_d
            relative_y = np.sin(laser_pose[2]) * free_d

            x = trans_laser[0] + relative_x
            y = trans_laser[1] + relative_y
            free_pixel = self.coordinate_to_pixel(x, y)
            free_pixel.reverse()
            free_pixel = tuple(free_pixel)

            laser_mask = np.full((self.map_height, self.map_width), 0, np.uint8)
            for index in range(len(distances)):
                distance = distances[index]
                if math.isnan(distance):
                    distance = laser_message.range_min
                if math.isinf(distance):
                    distance = laser_message.range_max
                if distance > laser_message.range_max:
                    distance = laser_message.range_max
                angle = angle_min + angle_increment * index + self.robot_pose[2]

                relative_x = np.cos(angle) * distance
                relative_y = np.sin(angle) * distance

                x = trans_laser[0] + relative_x
                y = trans_laser[1] + relative_y

                pixel = self.coordinate_to_pixel(x, y)
                pixel.reverse()
                pixel = tuple(pixel)
                if previous_pixel == pixel:
                    continue
                else:
                    if previous_pixel is None:
                        # cv2.line(laser_mask, self.s(laser_pixel), self.s(pixel), 255, 1)
                        # print(laser_pixel, pixel)
                        pass
                    else:
                        # cv2.line(laser_mask, self.s(previous_pixel), self.s(pixel), 255, 1)
                        # print(previous_pixel, pixel)
                        pass
                    line_pixels.append(pixel)
                    previous_pixel = pixel
                # cv2.circle(output_image, pixel, 1, (0, 200, 255), -1)
                # output_image[pixel[1], pixel[0]] = (0, 200, 255)

            # if pixel is not None:
            #     print(pixel, laser_pixel)
            #     cv2.line(laser_mask, self.s(pixel), self.s(laser_pixel), 255, 1)

            if len(line_pixels) > 3:
                mask_image = np.full((self.map_height, self.map_width), 255, np.uint8)
                for i in range(len(line_pixels)):
                    line_pixel = self.s(line_pixels[i])
                    if i + 1 < len(line_pixels):
                        next_pixel = self.s(line_pixels[i + 1])
                    else:
                        next_pixel = self.s(line_pixels[0])
                    cv2.line(laser_mask, line_pixel, next_pixel, 255, 1)

                # kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
                # laser_mask = cv2.dilate(laser_mask, kernel, iterations=2)

                # (981, 1139)
                new_mask = np.insert(laser_mask, 0, values=0, axis=0)
                new_mask = np.insert(new_mask, 0, values=0, axis=1)
                new_mask = np.insert(new_mask, len(new_mask), values=0, axis=0)
                new_mask = np.insert(new_mask, len(new_mask[0]), values=0, axis=1)

                cv2.floodFill(mask_image, new_mask, free_pixel, 0, cv2.FLOODFILL_MASK_ONLY)
                mask_image[np.logical_and(mask_image == 255, laser_mask == 255)] = 0
                output_image[mask_image == 255] = (0, 200, 255)

                self.laser_mask = new_mask

            # draw the pixel of robot
            cv2.circle(output_image, (robot_pixel[1], robot_pixel[0]), 4, (0, 255, 0), -1)
            self.output_image = output_image
            time.sleep(0.05)

            # break

    @staticmethod
    def search_pixels(condition):
        pixels = []
        pixels_search = np.where(condition)
        for index in range(len(pixels_search[0])):
            pixels.append((pixels_search[0][index], pixels_search[1][index]))
        return pixels

    def show(self):
        while True:
            if self.output_image is not None and self.laser_mask is not None:
                cv2.namedWindow("map", cv2.WINDOW_NORMAL)
                cv2.imshow("map", self.output_image)
                cv2.waitKey(50)

                cv2.namedWindow("laser", cv2.WINDOW_NORMAL)
                cv2.imshow("laser", self.laser_mask)
                cv2.waitKey(50)
            else:
                time.sleep(1)


def main():
    get_scanning = GetScanning()
    get_scanning.load()
    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()
