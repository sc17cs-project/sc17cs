#!/usr/bin/env python

import time
import cv2
from nav_msgs.msg import OccupancyGrid
import numpy as np
import rospy
from geometry_msgs.msg import Point
import os
import threading
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped


class ViewPlan:
    def __init__(self):
        self.resolution = 0.0
        self.robot_radius = 0.27
        self.origin = None  # type: Point
        self.map_height = 0
        self.map_width = 0
        self.map_image = None
        self.output_image = None
        self.robot_pose = (0, 0)
        rospy.on_shutdown(self.shutdown)
        rospy.init_node('view_plan', anonymous=True)

    @staticmethod
    def shutdown():
        # noinspection PyProtectedMember
        os._exit(0)

    def start(self):
        map_message = rospy.wait_for_message("/map", OccupancyGrid, 3)  # type: OccupancyGrid
        self.map_width = map_message.info.width
        self.map_height = map_message.info.height
        self.resolution = map_message.info.resolution
        self.origin = map_message.info.origin.position
        map_data = np.reshape(map_message.data, (self.map_width, self.map_height))
        rgb_image = np.zeros((self.map_height, self.map_width, 3), np.uint8)
        rgb_image.fill(205)
        for row in range(self.map_height):
            for col in range(self.map_width):
                probability = map_data[row, col]
                if probability == -1:
                    continue
                if probability > 0:
                    color = 0
                else:
                    color = 255
                # color = (1 - probability) * 255
                rgb_image[row, col] = (color, color, color)
        self.map_image = cv2.flip(rgb_image, 0)

        rospy.Subscriber("move_base/GlobalPlanner/plan", Path, self._callback)

        show_thread = threading.Thread(target=self.show)
        show_thread.daemon = True
        show_thread.start()

    def pixel_to_coordinate(self, row, col):
        return [col * self.resolution + self.origin.x, (self.map_height - row) * self.resolution + self.origin.y]

    def coordinate_to_pixel(self, x, y):
        return [self.map_height - int(round(((y - self.origin.y) / self.resolution))),
                int(round((x - self.origin.x) / self.resolution))]

    def _callback(self, data):
        """

        :type data: Path
        """
        output_image = self.map_image.copy()
        for pose_stamped in data.poses:  # type: PoseStamped
            pose = pose_stamped.pose
            pixel = self.coordinate_to_pixel(pose.position.x, pose.position.y)
            output_image[pixel[0], pixel[1]] = (0, 0, 255)
        self.output_image = output_image

    def show(self):
        while True:
            if self.output_image is not None:
                cv2.namedWindow("map", cv2.WINDOW_NORMAL)
                cv2.imshow("map", self.output_image)
                cv2.waitKey(50)
            else:
                time.sleep(1)


def main():
    view_plan = ViewPlan()
    view_plan.start()
    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()
