#!/usr/bin/env python
import math
import time
import cv2
from nav_msgs.msg import OccupancyGrid
import numpy as np
import rospy
from geometry_msgs.msg import Point
import tf
import os
import threading
from sensor_msgs.msg import LaserScan


class GetScanning:
    def __init__(self):
        self.resolution = 0.0
        self.robot_radius = 0.27
        self.origin = None  # type: Point
        self.map_height = 0
        self.map_width = 0
        self.map_image = None
        self.output_image = None
        self.listener = None
        self.robot_pose = (0, 0)
        rospy.on_shutdown(self.shutdown)
        rospy.init_node('scanning_image', anonymous=True)

    @staticmethod
    def shutdown():
        # noinspection PyProtectedMember
        os._exit(0)

    def load(self):
        map_message = rospy.wait_for_message("/map", OccupancyGrid, 3)  # type: OccupancyGrid
        self.map_width = map_message.info.width
        self.map_height = map_message.info.height
        self.resolution = map_message.info.resolution
        self.origin = map_message.info.origin.position
        map_data = np.reshape(map_message.data, (self.map_width, self.map_height))
        rgb_image = np.zeros((self.map_height, self.map_width, 3), np.uint8)
        rgb_image.fill(205)
        for row in range(self.map_height):
            for col in range(self.map_width):
                probability = map_data[row, col]
                if probability == -1:
                    continue
                if probability > 0:
                    color = 0
                else:
                    color = 255
                # color = (1 - probability) * 255
                rgb_image[row, col] = (color, color, color)
        self.map_image = cv2.flip(rgb_image, 0)

        self.listener = tf.TransformListener()
        print("Ready to start tf listener...")
        time.sleep(3)

        update_thread = threading.Thread(target=self.update)
        update_thread.daemon = True
        update_thread.start()

        show_thread = threading.Thread(target=self.show)
        show_thread.daemon = True
        show_thread.start()

    def pixel_to_coordinate(self, row, col):
        return [col * self.resolution + self.origin.x, (self.map_height - row) * self.resolution + self.origin.y]

    def coordinate_to_pixel(self, x, y):
        return [self.map_height - int(round(((y - self.origin.y) / self.resolution))),
                int(round((x - self.origin.x) / self.resolution))]

    @staticmethod
    def euler_from_quaternion(quaternion):
        # noinspection PyUnresolvedReferences
        return tf.transformations.euler_from_quaternion(quaternion)  # type: list

    def update(self):
        while True:
            # Get data from the laser sensor
            (trans, quaternion) = self.listener.lookupTransform('/map', '/base_link', rospy.Time(0))
            x = trans[0]
            y = trans[1]
            euler = self.euler_from_quaternion(quaternion)
            yaw = euler[2]
            self.robot_pose = (x, y, yaw)

            output_image = self.map_image.copy()
            robot_pixel = self.coordinate_to_pixel(self.robot_pose[0], self.robot_pose[1])

            (trans_laser, quaternion_laser) = self.listener.lookupTransform('/map', '/base_laser_link', rospy.Time(0))
            laser_message = rospy.wait_for_message("/scan", LaserScan, 1)  # type: LaserScan
            distances = laser_message.ranges
            angle_min = laser_message.angle_min
            angle_increment = laser_message.angle_increment

            for index in range(len(distances)):
                distance = distances[index]
                if math.isinf(distance) or math.isnan(distance):
                    continue
                if distance > laser_message.range_max or distance < laser_message.range_min:
                    continue
                angle = angle_min + angle_increment * index + self.robot_pose[2]

                relative_x = np.cos(angle) * distance
                relative_y = np.sin(angle) * distance

                x = trans_laser[0] + relative_x
                y = trans_laser[1] + relative_y

                pixel = self.coordinate_to_pixel(x, y)
                pixel.reverse()
                output_image[pixel[1], pixel[0]] = (0, 200, 255)
                # cv2.circle(output_image, tuple(pixel), 1, (0, 200, 255), -1)

            (trans_depth, quaternion_laser) = self.listener.lookupTransform('/map', '/rgbd_laser_link', rospy.Time(0))

            euler = self.euler_from_quaternion(quaternion_laser)
            yaw = euler[2]

            # Get data from RGBD camera
            laser_message = rospy.wait_for_message("/rgbd_scan", LaserScan, 1)  # type: LaserScan
            distances = laser_message.ranges
            angle_min = laser_message.angle_min
            angle_increment = laser_message.angle_increment
            range_min = laser_message.range_min
            range_max = laser_message.range_max

            for index in range(len(distances)):
                distance = distances[index]
                if math.isinf(distance) or math.isnan(distance):
                    continue
                if math.isnan(distance):
                    distance = range_min
                if math.isinf(distance):
                    distance = range_max
                if distance > range_max:
                    distance = range_max
                angle = angle_min + angle_increment * index + yaw

                relative_x = np.cos(angle) * distance
                relative_y = np.sin(angle) * distance

                x = trans_depth[0] + relative_x
                y = trans_depth[1] + relative_y

                pixel = self.coordinate_to_pixel(x, y)
                pixel.reverse()
                output_image[pixel[1],pixel[0]] = (0, 0, 255)
                # cv2.circle(output_image, tuple(pixel), 1, (0, 0, 255), -1)

            # draw the pixel of robot
            cv2.circle(output_image, (robot_pixel[1], robot_pixel[0]), 4, (0, 255, 0), -1)
            self.output_image = output_image
            time.sleep(0.05)

    @staticmethod
    def search_pixels(condition):
        """
        Search pixel
        :param condition:
        :return:
        """
        pixels = []
        pixels_search = np.where(condition)
        for index in range(len(pixels_search[0])):
            pixels.append((pixels_search[0][index], pixels_search[1][index]))
        return pixels

    def show(self):
        while True:
            if self.output_image is not None:
                cv2.namedWindow("map", cv2.WINDOW_NORMAL)
                cv2.imshow("map", self.output_image)
                cv2.waitKey(50)
            else:
                time.sleep(1)


def main():
    get_scanning = GetScanning()
    get_scanning.load()
    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()
