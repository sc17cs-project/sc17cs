import numpy as np
from multiprocessing import Process
from numpy import ndarray
from geometry_msgs.msg import Point


class Occlusion(Process):

    # noinspection PyPep8Naming
    def __init__(self, occlusion_queue, detected_pixels, elevator_image, ELEVATOR_AREA_COLOR, WAIT_AREA_COLOR,
                 trans_base, resolution, map_height, origin):
        super(Occlusion, self).__init__()
        self.occlusion_queue = occlusion_queue
        self.detected_pixels = detected_pixels
        self.elevator_image = elevator_image  # type: ndarray
        self.trans_base = trans_base
        self.ELEVATOR_AREA_COLOR = ELEVATOR_AREA_COLOR  # type: tuple
        self.WAIT_AREA_COLOR = WAIT_AREA_COLOR  # type: tuple
        self.resolution = resolution  # type: float
        self.map_height = map_height  # type: int
        self.origin = origin  # type: Point

    def run(self):
        for pixel, angle, distance in self.detected_pixels:
            if (self.elevator_image[pixel[0], pixel[1]] == self.ELEVATOR_AREA_COLOR).all():
                times = 1
                while True:
                    expand_result, expand_pixel = self.laser_expand(angle, self.trans_base, distance, times)
                    if expand_result is False:
                        break
                    else:
                        self.occlusion_queue.put(expand_pixel)
                    times += 1

            elif (self.elevator_image[pixel[0], pixel[1]] == self.WAIT_AREA_COLOR).all():
                times = 1
                elevator_detected = False
                while True:
                    try:
                        expand_result, expand_pixel = self.laser_expand(angle, self.trans_base, distance, times)
                        if expand_result is False:
                            current_color = self.elevator_image[expand_pixel[0], expand_pixel[1]]
                            if elevator_detected is True or (current_color != self.WAIT_AREA_COLOR).any():
                                break
                        else:
                            elevator_detected = True
                            self.occlusion_queue.put(expand_pixel)
                    except IndexError:
                        break
                    times += 1

            self.occlusion_queue.put(pixel)

    def laser_expand(self, angle, start_trans, start_distance, times):
        relative_x = np.cos(angle) * (start_distance + times * self.resolution)
        relative_y = np.sin(angle) * (start_distance + times * self.resolution)

        x = start_trans[0] + relative_x
        y = start_trans[1] + relative_y

        pixel = self.coordinate_to_pixel(x, y)
        if (self.elevator_image[pixel[0], pixel[1]] != self.ELEVATOR_AREA_COLOR).any():
            return False, pixel
        return True, pixel

    def coordinate_to_pixel(self, x, y=None):
        """
        :type x: list|Tuple|float
        :type y: None|float
        """
        if hasattr(x, '__len__') and len(x) >= 2 and y is None:
            y = x[1]
            x = x[0]
        return [self.map_height - int(round(((y - self.origin.y) / self.resolution))),
                int(round((x - self.origin.x) / self.resolution))]
