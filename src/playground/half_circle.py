import cv2
import numpy as np

# Colors (B, G, R)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)


def create_blank(width, height, color=(0, 0, 0)):
    """Create new image(numpy array) filled with certain color in BGR"""
    image = np.zeros((height, width, 3), np.uint8)
    # Fill image with color
    image[:] = color

    return image


def draw_half_circle_rounded(image):
    """
    Draw a semicircle
    :param image:
    :return:
    """
    height, width = image.shape[0:2]
    # Ellipse parameters
    radius = 100
    center = (width / 2, height / 2)
    axes = (radius, radius)
    angle = -90 - 110
    startAngle = 0
    endAngle = 220
    thickness = 10

    # http://docs.opencv.org/modules/core/doc/drawing_functions.html#ellipse
    cv2.ellipse(image, center, axes, angle, startAngle, endAngle, BLACK, -1)
    cv2.circle(image, center, 3, (0, 255, 0), -1)
    image = cv2.flip(image, 0)
    return image


# def draw_half_circle_rounded(image):
#     height, width = image.shape[0:2]
#     # Ellipse parameters
#     radius = 100
#     center = (width / 2, height / 2)
#     axes = (radius, radius)
#     angle = 0
#     robot_angle = -30
#     startAngle = robot_angle - 110
#     endAngle = 220 + startAngle
#     thickness = 10
#
#     # http://docs.opencv.org/modules/core/doc/drawing_functions.html#ellipse
#     cv2.ellipse(image, center, axes, angle, startAngle, endAngle, BLACK, -1)
#     cv2.circle(image, center, 3, (0, 255, 0), -1)
#     image = cv2.flip(image, 0)
#     return image


# Create new blank 300x150 white image
width, height = 400, 400
image = create_blank(width, height, color=WHITE)
image = draw_half_circle_rounded(image)
cv2.imshow('half_circle_rounded.jpg', image)
cv2.waitKey(0)
cv2.destroyAllWindows()
