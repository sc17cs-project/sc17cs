# coding=utf-8
import math

import cv2
import numpy as np

ELEVATOR_FREE_COLOR = (0, 255, 0)
ELEVATOR_RESTRICTED_COLOR = (0, 0, 255)
ELEVATOR_BOTTOM_COLOR = (255, 0, 0)
WAIT_AREA_COLOR = (0, 255, 255)
t = "../../maps/dec10/elevator.png"


def search_pixels(condition):
    pixels = []
    pixels_search = np.where(condition)
    for index in xrange(len(pixels_search[0])):
        pixels.append((pixels_search[1][index], pixels_search[0][index]))
    return pixels


elevator_rgb = cv2.imread(t)
# replace all 254 to 255
elevator_rgb[np.where((elevator_rgb >= (254, 254, 254)).all(axis=2))] = (255, 255, 255)
elevator_mask = cv2.inRange(elevator_rgb, ELEVATOR_FREE_COLOR, ELEVATOR_FREE_COLOR)
elevator_bottom_mask = cv2.inRange(elevator_rgb, ELEVATOR_FREE_COLOR, ELEVATOR_BOTTOM_COLOR)
elevator_mask = cv2.bitwise_or(elevator_mask, elevator_bottom_mask)
elevator_gray = cv2.cvtColor(
    cv2.bitwise_and(elevator_rgb, elevator_rgb, mask=elevator_mask), cv2.COLOR_BGR2GRAY)

_, contours, _ = cv2.findContours(elevator_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
max_contour = max(contours, key=cv2.contourArea)
moment = cv2.moments(max_contour)
default_elevator_pixel = (
    int(round(moment['m10'] / moment['m00'])),
    int(round(moment['m01'] / moment['m00']))
)
print("Elevator free center pixel (%d,%d)" % default_elevator_pixel)

waiting_mask = cv2.inRange(elevator_rgb, WAIT_AREA_COLOR, WAIT_AREA_COLOR)
waiting_gray = cv2.cvtColor(
    cv2.bitwise_and(elevator_rgb, elevator_rgb, mask=waiting_mask), cv2.COLOR_BGR2GRAY)
_, contours, _ = cv2.findContours(waiting_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
max_contour = max(contours, key=cv2.contourArea)
moment = cv2.moments(max_contour)
default_waiting_pixel = (
    int(round(moment['m10'] / moment['m00'])),
    int(round(moment['m01'] / moment['m00']))
)
print("Default waiting pixel (%d,%d)", default_waiting_pixel)

bottom_pixels = np.array(search_pixels((elevator_rgb == ELEVATOR_BOTTOM_COLOR).all(axis=2)))
elevator_bottom_vector = np.mean(bottom_pixels, axis=0).reshape((2, 1))
elevator_bottom_pixel = tuple([int(round(n)) for n in np.mean(np.array(bottom_pixels), axis=0).tolist()])

# Fit the inner boundary into a straight line
vx, vy, cx, cy = cv2.fitLine(bottom_pixels, cv2.DIST_L2, 0, 0.01, 0.01)

# Should big enough
m = 200

# line_start/line_end = (x,y) => (col,row)
line_start = (float(cx - m * vx[0]), float(cy - m * vy[0]))
line_end = (float(cx + m * vx[0]), float(cy + m * vy[0]))

# Fitting line [ line_a*x+ line_b*y + line_c = 0 ]
line_a = line_end[1] - line_start[1]
line_b = -1 * (line_end[0] - line_start[0]) + 1e-8
line_c = -1 * line_a * elevator_bottom_pixel[0] - line_b * elevator_bottom_pixel[1]
elevator_bottom_line = (line_a, line_b, line_c)
line_slope = -1 * line_a / line_b
line_slope_degree = math.degrees(math.atan(line_slope))
print ("Bottom line: %.4f*x + %.4f*y + %.4f = 0; Slope: %.4f, Slope degree: %.4f" % (line_a, line_b,
                                                                                     line_c, line_slope,
                                                                                     line_slope_degree))

mid_line_a = line_b
mid_line_b = line_a + 1e-8
mid_line_c = -1 * mid_line_a * elevator_bottom_pixel[0] - mid_line_b * elevator_bottom_pixel[1]
elevator_mid_line = (mid_line_a, mid_line_b, mid_line_c)
print("Mid line: %.4f*x + %.4f*y + %.4f = 0" % (mid_line_a, mid_line_b, mid_line_c))

if line_slope_degree == 0:
    enter_degree = 90.0 if default_elevator_pixel[1] > elevator_bottom_pixel[1] else -90.0
elif line_slope_degree < 0:
    if default_elevator_pixel[1] < elevator_bottom_pixel[1]:
        enter_degree = -1 * (90 + line_slope_degree)
    else:
        enter_degree = 90 - line_slope_degree
else:
    if default_elevator_pixel[1] < elevator_bottom_pixel[1]:
        enter_degree = -1 * (90 + line_slope_degree)
    else:
        enter_degree = 90 - line_slope_degree
enter_degree = round(enter_degree, 2)

print("enter_degree", enter_degree)
# cv2.imshow("t", image)
# cv2.waitKey(0)
# cv2.destroyWindow()
