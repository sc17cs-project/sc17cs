#!/usr/bin/env python

import rospy
from math import radians
from geometry_msgs.msg import Twist


def publisher():
    """
    Robot drawing circle
    :return:
    """
    pub = rospy.Publisher('mobile_base_controller/cmd_vel', Twist, queue_size=10)
    rospy.init_node('Walker', anonymous=True)
    rate = rospy.Rate(10)  # 10hz
    desired_velocity = Twist()
    desired_velocity.linear.x = 0.2  # Forward with 0.2 m/sec.
    desired_velocity.angular.z = radians(45)
    while not rospy.is_shutdown():
        pub.publish(desired_velocity)
        rate.sleep()
    pass


if __name__ == "__main__":
    try:
        publisher()
    except rospy.ROSInterruptException:
        pass
