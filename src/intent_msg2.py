import json
from enum import Enum, unique


@unique
class IntentType(Enum):
    GOAL_SET = 0
    GOAL_REACH = 1
    BUTTON_PRESSED = 2
    CONFIRM = 3
    REPEAT = 4
    NONE = 5


class IntentMsg:

    def __init__(self, intent_type, data=None):
        self.intent_type = intent_type  # type: IntentType
        self.data = data

    def json(self):
        """
        Convert a message to a JSON string
        :return:
        """
        return json.dumps({"intent_type": int(self.intent_type.value), "data": self.data})

    @staticmethod
    def load(json_string):
        """
        Convert a string to an IntentMsg
        :param json_string:
        :return:
        """
        data = json.loads(json_string)
        if "intent_type" in data and "data" in data:
            try:
                intent_type = IntentType(int(data['intent_type']))
                return IntentMsg(intent_type, data['data'])
            except ValueError:
                pass
        return IntentMsg(IntentType.NONE)

    def is_type(self, intent_type):
        return self.intent_type is intent_type
