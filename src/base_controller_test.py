from controllers.base_controller import BaseController
import rospy

rospy.init_node('base_controller_test', anonymous=True)
action = BaseController()
state = action.goto((6.600, 0.200), degrees=90)
print (state)
