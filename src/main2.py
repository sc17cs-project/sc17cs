#!/usr/bin/env python
import rospy
import time
from move2 import Move
import os
from status import Status
import std_msgs.msg as msg
import threading
from speaker import Speaker
from intent_msg import IntentMsg


class Main:

    def __init__(self):
        self.__move = Move()
        self.status = None  # type: Status
        self.goal = None  # type: int
        self.goal_ordinal = None  # type: str

        self.spoken_text = None  # type :str
        self.speaker = Speaker()

    def init(self):
        """
        Program initialization
        :return:
        """
        self.__move.init()

        if not self.__move.is_in_elevator():
            self.status = Status.WAITING_GOAL
        else:
            self.status = Status.WAITING_LEAVE_WITHOUT_GOAL
        start_thread = threading.Thread(target=self._start)
        start_thread.daemon = True
        start_thread.start()

        rospy.Subscriber("sc17cs/intent", msg.String, self._intent_callback)

    @staticmethod
    def _ordinal(number):
        """
        Convert to ordinal
        :param number:
        :return:
        """
        return "%d%s" % (number, "tsnrhtdd"[(number / 10 % 10 != 1) * (number % 10 < 4) * number % 10::4])

    def _speak(self, text):
        """
        Call speaker module
        :param text:
        :return:
        """
        self.spoken_text = text
        self.speaker.run(text)

    def _intent_callback(self, result):
        """
        Intent callback
        :param result:
        :return:
        """
        if hasattr(result, 'data'):
            intent = IntentMsg.load(result.data)
            if intent.is_type(IntentMsg.REPEAT):
                self._say_again()
            elif intent.is_type(IntentMsg.NONE):
                self.speaker.run("Sorry, I can not understand what you are saying.")

    def _say_again(self):
        if self.spoken_text is not None:
            self.speaker.run(self.spoken_text)

    def _wait_for_intent(self, intent_type, timeout=None):
        """
        Waiting for the specified intent
        :param intent_type:
        :param timeout:
        :return:
        """
        while True:
            result = rospy.wait_for_message("sc17cs/intent", msg.String, timeout)
            if hasattr(result, 'data'):
                intent = IntentMsg.load(result.data)
                if intent.is_type(intent_type):
                    return intent
                elif (not intent.is_type(IntentMsg.NONE)) and (not intent.is_type(IntentMsg.REPEAT)):
                    rospy.logwarn("Got wrong intent.")
                    self._say_again()

    def _start(self):
        try:
            while not rospy.is_shutdown():
                time.sleep(0.1)
                rospy.loginfo("Current status: %s", self.status)
                if self.status == Status.WAITING_GOAL:
                    self._speak("I am ready to accept the command to set the goal.")
                    intent = self._wait_for_intent(IntentMsg.GOAL_SET)
                    goal = intent.data
                    goal_ordinal = self._ordinal(goal)
                    self._speak("The goal is %s floor, right?" % goal_ordinal)
                    intent = self._wait_for_intent(IntentMsg.CONFIRM)
                    if not intent.data:
                        self._speak("You canceled the goal, I will wait for you to set a new one.")
                        continue
                    else:
                        self.goal = goal
                        self.goal_ordinal = goal_ordinal
                        self._speak("Goal confirmed, executing entering elevator procedure.")
                        self.status = Status.ENTERING_ELEVATOR
                        self.__move.enter()
                        self.status = Status.WAITING_BUTTON
                        self._speak(
                            "Could you help me press the button of the %s floor, thank you." % self.goal_ordinal)
                elif self.status == Status.WAITING_BUTTON:
                    self._wait_for_intent(IntentMsg.BUTTON_PRESSED)
                    self._speak("You have pressed the button, right?")
                    intent = self._wait_for_intent(IntentMsg.CONFIRM)
                    if not intent.data:
                        self._speak(
                            "Could you help me press the button of the %s floor, thank you." % self.goal_ordinal)
                        continue
                    else:
                        self._speak("Ok, I will wait for the command for leaving the elevator.")
                        self.status = Status.WAITING_LEAVE

                elif self.status == Status.WAITING_LEAVE_WITHOUT_GOAL:
                    self._speak("I am ready to accept the command to leave the elevator.")
                    self.status = Status.WAITING_LEAVE
                elif self.status == Status.WAITING_LEAVE:
                    self._wait_for_intent(IntentMsg.GOAL_REACH)
                    if self.goal_ordinal is not None:
                        self._speak("We arrived at %s floor, right?" % self.goal_ordinal)
                    else:
                        self._speak("We arrived at the goal, right?")
                    intent = self._wait_for_intent(IntentMsg.CONFIRM)
                    if not intent.data:
                        self._speak("Ok, I will continue to wait for the command for leaving the elevator.")
                        continue
                    else:
                        self._speak("Goal is reached, executing leaving elevator procedure.")
                        self.status = Status.LEAVING_ELEVATOR
                        self.__move.leave()
                        self.status = Status.FINISHED
                        self._speak("I have successfully leaved the elevator.")
                elif self.status == Status.FINISHED:
                    time.sleep(5)

        except rospy.ROSException as e:
            rospy.logerr(e.message)
            shutdown()


def shutdown():
    rospy.logwarn("Shutdown...")
    # noinspection PyProtectedMember
    os._exit(0)


def main():
    rospy.on_shutdown(shutdown)
    rospy.init_node('sc17cs_main')

    __main = Main()
    __main.init()

    # response the ctrl+c
    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()
