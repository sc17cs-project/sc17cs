#!/usr/bin/env python

import rospy
from parking_pose import ParkingPose
from controllers.abase_controller import BaseController
import std_msgs.msg as msg
import os
import time
import threading


class Base:
    def __init__(self):
        rospy.on_shutdown(self.shutdown)
        rospy.init_node('sc17cs_base', anonymous=True)
        self.current_parking = None  # type: ParkingPose
        self.controller = BaseController()
        self.controller.reg_callback(self.callback)
        self.finished = False

    def start(self):
        subscriber_thread = threading.Thread(target=self.subscriber_parking)
        subscriber_thread.daemon = True
        subscriber_thread.start()

    def callback(self, state):
        if state is True:
            if self.current_parking.priority == ParkingPose.ELEVATOR:
                self.finished = True
            else:
                rospy.loginfo("Waiting for new parking pose")

    @staticmethod
    def shutdown():
        rospy.logwarn("Shutdown...")
        # noinspection PyProtectedMember
        os._exit(0)

    def subscriber_parking(self):
        while not rospy.is_shutdown():
            message = rospy.wait_for_message("/sc17cs/parking", msg.String, 5)
            if hasattr(message, "data"):
                __parking = ParkingPose.load(message.data)
                pose = __parking.pose
                if self.finished is True:
                    if self.current_parking.pose == pose:
                        rospy.loginfo("Successfully entered the elevator")
                        self.shutdown()
                        break
                    else:
                        self.finished = False
                if self.current_parking is None or self.current_parking.pose != pose:
                    self.controller.goto(pose[:2], degrees=pose[2])
                    self.current_parking = __parking
                    rospy.loginfo("New parking %s", __parking)


def main():
    __base = Base()
    __base.start()
    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()
