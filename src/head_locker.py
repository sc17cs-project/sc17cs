#!/usr/bin/env python

import rospy
from controllers.head_controller import HeadController


def publisher():
    rospy.init_node('sc17cs_head_locker', anonymous=True)
    head = HeadController()
    rate = rospy.Rate(2)  # 10hz
    while not rospy.is_shutdown():
        head.goto(0, 0)
        rate.sleep()


if __name__ == "__main__":
    try:
        publisher()
    except rospy.ROSInterruptException:
        pass
