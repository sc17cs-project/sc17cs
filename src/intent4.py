#!/usr/bin/env python
import rospy
import socket
import os
import std_msgs.msg
import time
import threading
import requests
from requests import RequestException
import json
from intent_msg2 import IntentMsg, IntentType


class Intent:
    def __init__(self, ip, port, speech_server):
        self.intent_publisher = rospy.Publisher('sc17cs/intent', std_msgs.msg.String, queue_size=5)
        self.client = None  # type:socket.socket
        self.ip = ip
        self.port = port
        self.speech_server = speech_server

    def start(self):
        rospy.loginfo("Try to connect STT server, IP: %s, Port: %d", self.ip, self.port)
        self.client = socket.socket()
        self.client.settimeout(30)
        try:
            self.client.connect((self.ip, self.port))
        except socket.error:
            rospy.logerr("STT Connected Failed")
            shutdown()
        rospy.loginfo("STT Connected successfully")

        heart_thread = threading.Thread(target=self._heart)
        heart_thread.daemon = True
        heart_thread.start()

        while True:
            try:
                message = self.client.makefile().readline().strip("\n").strip()
                if len(message) > 0:
                    rospy.loginfo("Get message: %s", message)
                    self._pub_intent(message)
            except socket.timeout:
                pass
            time.sleep(0.5)

    def _heart(self):
        """
        Heartbeat package
        :return:
        """
        while True:
            try:
                self.client.sendall("ping".encode())
            except socket.error:
                rospy.logerr("STT server is down")
                shutdown()
            time.sleep(3)

    def _pub_intent(self, message):
        """
        Release intention
        :param message:
        :return:
        """
        try:
            response = requests.post(self.speech_server + "/intent", json={
                'message': message
            })
            try:
                if response.status_code == 200:
                    data = response.json()
                    rospy.loginfo("LUIS Result: %s", json.dumps(data))
                    if 'topScoringIntent' in data:
                        intent = data['topScoringIntent']
                        if intent['score'] > 0.5:
                            intent_name = intent['intent']
                            if intent_name == 'Goal.Set':
                                self._pub_goal_set(data['entities'])
                                return
                            elif intent_name == 'Goal.Reach':
                                self._pub_goal_reach()
                                return
                            elif intent_name == 'Button.Pressed':
                                self._pub_button_pressed()
                                return
                            elif intent_name == 'Utilities.Cancel':
                                self._pub_cancel()
                                return
                            elif intent_name == 'Utilities.Confirm':
                                self._pub_confirm()
                                return
                            elif intent_name == 'Utilities.Repeat':
                                self._pub_repeat()
                                return
                        self._pub_none()
                        return
                else:
                    rospy.logerr("LUIS error: %d", response.status_code)

            except ValueError as e:
                rospy.logerr(e.message)

        except RequestException as e:
            rospy.logerr(e.message)

    def _pub_goal_set(self, entities):
        if len(entities) > 0:
            entity = entities[0]
            if entity['type'] == 'builtin.number' or entity['type'] == 'builtin.ordinal':
                value = int(entity['resolution']['value'])
                rospy.loginfo("Intent.GOAL_SET is published: %d", value)
                self.intent_publisher.publish(IntentMsg(IntentType.GOAL_SET, value).json())

    def _pub_goal_reach(self):
        rospy.loginfo('Intent.GOAL_REACH is published')
        self.intent_publisher.publish(IntentMsg(IntentType.GOAL_REACH).json())

    def _pub_button_pressed(self):
        rospy.loginfo('Intent.BUTTON_PRESSED is published')
        self.intent_publisher.publish(IntentMsg(IntentType.BUTTON_PRESSED).json())

    def _pub_cancel(self):
        rospy.loginfo('Intent.CANCEL is published')
        self.intent_publisher.publish(IntentMsg(IntentType.CONFIRM, False).json())

    def _pub_confirm(self):
        rospy.loginfo('Intent.CONFIRM is published')
        self.intent_publisher.publish(IntentMsg(IntentType.CONFIRM, True).json())

    def _pub_repeat(self):
        rospy.loginfo('Intent.REPEAT is published')
        self.intent_publisher.publish(IntentMsg(IntentType.REPEAT).json())

    def _pub_none(self):
        rospy.loginfo('Intent.NONE is published')
        self.intent_publisher.publish(IntentMsg(IntentType.NONE).json())


def shutdown():
    rospy.logwarn("Shutdown...")
    # noinspection PyProtectedMember
    os._exit(0)


def main():
    rospy.on_shutdown(shutdown)
    rospy.init_node('sc17cs_intent')

    params = {
        'ip': rospy.get_param('~stt_server', '127.0.0.1'),
        'port': rospy.get_param('~stt_port', 1201),
        'speech_server': rospy.get_param('~speech_server', 'http://127.0.0.1:1221'),
    }
    rospy.loginfo("Intent params: %s", json.dumps(params))

    __pub = Intent(**params)
    __pub.start()


if __name__ == '__main__':
    main()
