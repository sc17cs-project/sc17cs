#!/usr/bin/env python
import rospy
import time
from move2 import Move
import os
from status import Status
import std_msgs.msg as msg
import threading
from speaker import Speaker


class Main:

    def __init__(self):
        self.__move = Move()
        self.status = None  # type: Status
        self.goal = None  # type: int
        self.goal_ordinal = None  # type: str

        self.spoken_text = None  # type :str
        self.speaker = Speaker()

    def init(self):
        self.__move.init()

        if not self.__move.is_in_elevator():
            self.status = Status.WAITING_GOAL
        else:
            self.status = Status.WAITING_LEAVE_WITHOUT_GOAL
        start_thread = threading.Thread(target=self._start)
        start_thread.daemon = True
        start_thread.start()

        rospy.Subscriber("sc17cs/intent/repeat", msg.Bool, self._intent_repeat)
        rospy.Subscriber("sc17cs/intent/none", msg.Bool, self._intent_none)

    @staticmethod
    def _ordinal(number):
        return "%d%s" % (number, "tsnrhtdd"[(number / 10 % 10 != 1) * (number % 10 < 4) * number % 10::4])

    def _speak(self, text):
        """
        Call speaker module
        :param text:
        :return:
        """
        self.spoken_text = text
        self.speaker.run(text)

    def _intent_repeat(self, data):
        """
        When the repeat intention is received
        :param data:
        :return:
        """
        if self.spoken_text is not None:
            self.speaker.run(self.spoken_text)

    def _intent_none(self, data):
        """
        When the None intention is received
        :param data:
        :return:
        """
        if self.spoken_text is not None:
            self.speaker.run("Sorry, I can not understand what you are saying.")

    @staticmethod
    def _wait_for_std_message(topic_name, data_type, timeout=None):
        """
        Waiting for std message
        :param topic_name:
        :param data_type:
        :param timeout:
        :return:
        """
        result = rospy.wait_for_message(topic_name, data_type, timeout)
        if hasattr(result, 'data'):
            return result.data
        raise rospy.ROSException("std topic [%s] response dose not have data attribute." % topic_name)

    def _start(self):
        """
        Task start
        :return:
        """
        try:
            while not rospy.is_shutdown():
                time.sleep(0.1)
                rospy.loginfo("Current status: %s", self.status)
                if self.status == Status.WAITING_GOAL:
                    self._speak("I am ready to accept the command to set the goal.")
                    goal = self._wait_for_std_message('sc17cs/intent/goal_set', msg.Int16)  # type: int
                    goal_ordinal = self._ordinal(goal)
                    self._speak("The goal is %s floor, right?" % goal_ordinal)
                    confirm = self._wait_for_std_message('sc17cs/intent/confirm', msg.Bool)  # type: bool
                    if not confirm:
                        self._speak("You canceled the goal, I will wait for you to set a new one.")
                        continue
                    else:
                        self.goal = goal
                        self.goal_ordinal = goal_ordinal
                        self._speak("Goal confirmed, executing entering elevator procedure.")
                        self.status = Status.ENTERING_ELEVATOR
                        self.__move.enter()
                        self.status = Status.WAITING_BUTTON
                        self._speak(
                            "Could you help me press the button of the %s floor, thank you." % self.goal_ordinal)
                elif self.status == Status.WAITING_BUTTON:
                    self._wait_for_std_message('sc17cs/intent/button_pressed', msg.Bool)
                    self._speak("You have pressed the button, right?")
                    confirm = self._wait_for_std_message('sc17cs/intent/confirm', msg.Bool)  # type: bool
                    if not confirm:
                        self._speak(
                            "Could you help me press the button of the %s floor, thank you." % self.goal_ordinal)
                        continue
                    else:
                        self._speak("Ok, I will wait for the command for leaving the elevator.")
                        self.status = Status.WAITING_LEAVE

                elif self.status == Status.WAITING_LEAVE_WITHOUT_GOAL:
                    self._speak("I am ready to accept the command to leave the elevator.")
                    self.status = Status.WAITING_LEAVE
                elif self.status == Status.WAITING_LEAVE:
                    self._wait_for_std_message('sc17cs/intent/goal_reach', msg.Bool)
                    if self.goal_ordinal is not None:
                        self._speak("We arrived at %s floor, right?" % self.goal_ordinal)
                    else:
                        self._speak("We arrived at the goal, right?")
                    confirm = self._wait_for_std_message('sc17cs/intent/confirm', msg.Bool)  # type: bool
                    if not confirm:
                        self._speak("Ok, I will continue to wait for the command for leaving the elevator.")
                        continue
                    else:
                        self._speak("Goal is reached, executing leaving elevator procedure.")
                        self.status = Status.LEAVING_ELEVATOR
                        self.__move.leave()
                        self.status = Status.FINISHED
                        self._speak("I have successfully left the elevator.")
                elif self.status == Status.FINISHED:
                    time.sleep(5)

        except rospy.ROSException as e:
            rospy.logerr(e.message)
            shutdown()


def shutdown():
    rospy.logwarn("Shutdown...")
    # noinspection PyProtectedMember
    os._exit(0)


def main():
    rospy.on_shutdown(shutdown)
    rospy.init_node('sc17cs_main')

    __main = Main()
    __main.init()

    # response the ctrl+c
    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()
