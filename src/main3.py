#!/usr/bin/env python
import rospy
import time
from move3 import Move
import os
from status import Status
import std_msgs.msg as msg
import threading
from speaker import Speaker
from intent_msg2 import IntentMsg, IntentType


class Main:

    def __init__(self):
        self.__move = Move()
        self.status = None  # type: Status
        self.goal = None  # type: int
        self.goal_ordinal = None  # type: str

        self.spoken_text = None  # type :str
        self.speaker = Speaker()

    def init(self):
        """
        Program initialization
        :return:
        """
        self.__move.init()

        if not self.__move.is_in_elevator():
            self.status = Status.WAITING_GOAL
        else:
            self.status = Status.WAITING_LEAVE_WITHOUT_GOAL
        start_thread = threading.Thread(target=self._start)
        start_thread.daemon = True
        start_thread.start()

        rospy.Subscriber("sc17cs/intent", msg.String, self._intent_callback)

    @staticmethod
    def _ordinal(number):
        """
        Convert to ordinal
        :param number:
        :return:
        """
        return "%d%s" % (number, "tsnrhtdd"[(number / 10 % 10 != 1) * (number % 10 < 4) * number % 10::4])

    def _speak(self, text):
        """
        Call speaker module
        :param text:
        :return:
        """
        self.spoken_text = text
        self.speaker.run(text)

    def _intent_callback(self, result):
        """
        Intent callback
        :param result:
        :return:
        """
        if hasattr(result, 'data'):
            intent = IntentMsg.load(result.data)
            if intent.is_type(IntentType.REPEAT) or intent.is_type(IntentType.NONE):
                self._say_again()
            # elif intent.is_type(IntentType.NONE):
            #     self.speaker.run("Sorry, I can not understand what you are saying.")
            #     self._say_again()

    def _say_again(self):
        """
        Repeat it again
        :return:
        """
        if self.spoken_text is not None:
            self.speaker.run(self.spoken_text)

    def _wait_for_intent(self, intent_types, timeout=None):
        """
        Waiting for the specified intent
        :param intent_types:
        :param timeout:
        :return:
        """
        if not hasattr(intent_types, "__len__"):
            intent_types = [intent_types]
        while True:
            result = rospy.wait_for_message("sc17cs/intent", msg.String, timeout)
            if hasattr(result, 'data'):
                intent = IntentMsg.load(result.data)
                for intent_type in intent_types:
                    if intent.is_type(intent_type):
                        return intent

                if intent.is_type(IntentType.NONE) is False and intent.is_type(IntentType.REPEAT) is False:
                    rospy.logwarn("Got wrong intent.")
                    self._say_again()

    def _is_waiting_elevator(self, is_waiting):
        """
        Is the robot waiting for the elevator?
        :param is_waiting:
        :return:
        """
        if self.status is Status.ENTERING_ELEVATOR and is_waiting is True:
            self.status = Status.WAITING_ELEVATOR
            self._speak("Can you help me call the elevator, thank you.")
        elif self.status is Status.WAITING_ELEVATOR and is_waiting is False:
            self.status = Status.ENTERING_ELEVATOR

    def _start(self):
        try:
            while not rospy.is_shutdown():
                time.sleep(0.1)
                rospy.loginfo("Current status: %s", self.status)
                if self.status == Status.WAITING_GOAL:
                    self._speak("I am ready to accept the command to set the goal.")
                    intent = self._wait_for_intent(IntentType.GOAL_SET)
                    goal = intent.data
                    goal_ordinal = self._ordinal(goal)
                    while True:
                        self._speak("The goal is %s floor, right?" % goal_ordinal)
                        intent = self._wait_for_intent((IntentType.CONFIRM, IntentType.GOAL_SET))

                        if intent.is_type(IntentType.CONFIRM):
                            if not intent.data:
                                self._speak("You canceled the goal, I will wait for you to set a new one.")
                                intent = self._wait_for_intent(IntentType.GOAL_SET)
                                goal = intent.data
                                goal_ordinal = self._ordinal(goal)
                                continue
                            else:
                                break
                        elif intent.is_type(IntentType.GOAL_SET):
                            goal = intent.data
                            goal_ordinal = self._ordinal(goal)
                            continue

                    self.goal = goal
                    self.goal_ordinal = goal_ordinal
                    self._speak("Goal confirmed, executing entering elevator procedure.")
                    self.status = Status.ENTERING_ELEVATOR
                    self.__move.enter(self._is_waiting_elevator)
                    self.status = Status.WAITING_BUTTON

                elif self.status == Status.WAITING_BUTTON:
                    while True:
                        self._speak(
                            "Could you help me press the button of the %s floor and tell me after you pressed it, thank you." % self.goal_ordinal)
                        intent = self._wait_for_intent((IntentType.BUTTON_PRESSED, IntentType.CONFIRM))
                        if intent.is_type(IntentType.CONFIRM) and intent.data is False:
                            continue

                        self._speak("You have pressed the button, right?")
                        intent = self._wait_for_intent(IntentType.CONFIRM)
                        if not intent.data:
                            continue
                        else:
                            break
                    self._speak("Ok, please tell me when the elevator reaches the %s floor." % self.goal_ordinal)
                    self.status = Status.WAITING_LEAVE

                elif self.status == Status.WAITING_LEAVE_WITHOUT_GOAL:
                    self._speak("I am ready to accept the command to leave the elevator.")
                    self.status = Status.WAITING_LEAVE
                elif self.status == Status.WAITING_LEAVE:
                    self._wait_for_intent(IntentType.GOAL_REACH)
                    if self.goal_ordinal is not None:
                        self._speak("We arrived at %s floor, right?" % self.goal_ordinal)
                    else:
                        self._speak("We arrived at the goal, right?")
                    intent = self._wait_for_intent(IntentType.CONFIRM)
                    if not intent.data:
                        self._speak("Ok, I will continue to wait for the command for leaving the elevator.")
                        continue
                    else:
                        self._speak("Goal is reached, executing leaving elevator procedure.")
                        self.status = Status.LEAVING_ELEVATOR
                        self.__move.leave()
                        self.status = Status.FINISHED
                        self._speak("I have successfully leaved the elevator.")
                elif self.status == Status.FINISHED:
                    time.sleep(5)

        except rospy.ROSException as e:
            rospy.logerr(e.message)
            shutdown()


def shutdown():
    rospy.logwarn("Shutdown...")
    # noinspection PyProtectedMember
    os._exit(0)


def main():
    rospy.on_shutdown(shutdown)
    rospy.init_node('sc17cs_main')

    __main = Main()
    __main.init()

    # response the ctrl+c
    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()
