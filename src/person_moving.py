#!/usr/bin/env python
from __future__ import print_function

import rospy
import time
import os
from geometry_msgs.msg import Point, Pose
from gazebo_msgs.msg import ModelState, ModelStates
import tf

__DIR__ = os.path.split(os.path.realpath(__file__))[0]


class PersonMoving:
    def __init__(self):
        self.pub_model = rospy.Publisher('gazebo/set_model_state', ModelState, queue_size=1)

        self.person1_points = self.get_points(os.path.join(__DIR__, "../", "records/person_moving_1_1552421546.txt"))
        self.person2_points = self.get_points(os.path.join(__DIR__, "../", "records/person_moving_2_1552421267.txt"))
        self.person3_points = self.get_points(os.path.join(__DIR__, "../", "records/person_moving_3_1552421441.txt"))
        self.person4_points = self.get_points(os.path.join(__DIR__, "../", "records/person_moving_4_1552421601.txt"))

    @staticmethod
    def get_points(file_path):
        points = []
        with open(file_path, "r") as f:
            for line in f.readlines():
                line = line.rstrip("\n")
                point = line.split(",")
                if len(point) == 3:
                    points.append(tuple([float(number) for number in point]))
        return points

    @staticmethod
    def _euler_from_quaternion(quaternion):
        # noinspection PyUnresolvedReferences
        return tf.transformations.euler_from_quaternion(quaternion)  # type: list

    @staticmethod
    def _euler_to_quaternion(euler):
        # noinspection PyUnresolvedReferences
        return tf.transformations.quaternion_from_euler(euler[0], euler[1], euler[2])  # type: list

    def start(self):
        """
        Move the dummy to the correct position
        :return:
        """
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            model = rospy.wait_for_message('gazebo/model_states', ModelStates)  # type: ModelStates
            for index in range(len(model.name)):
                name = model.name[index]
                pose = model.pose[index]  # type: Pose

                if name == "person_moving_1" and len(self.person1_points) > 0:
                    self.pub(self.person1_points.pop(0), name, pose)
                    continue
                elif name == "person_moving_2" and len(self.person2_points) > 0:
                    self.pub(self.person2_points.pop(0), name, pose)
                    continue
                elif name == "person_moving_3" and len(self.person3_points) > 0:
                    self.pub(self.person3_points.pop(0), name, pose)
                    continue
                elif name == "person_moving_4" and len(self.person4_points) > 0:
                    self.pub(self.person4_points.pop(0), name, pose)
                    continue
                if len(self.person1_points) == 0 and \
                        len(self.person2_points) == 0 and \
                        len(self.person3_points) == 0 and \
                        len(self.person4_points) == 0:
                    shutdown()

            rate.sleep()

    def pub(self, point, name, pose):
        quaternion = pose.orientation
        person = ModelState()
        person.model_name = name
        person.pose = pose
        person.pose.position.x = point[0]
        person.pose.position.y = point[1]

        euler = self._euler_from_quaternion([quaternion.x, quaternion.y, quaternion.z, quaternion.w])
        euler = list(euler)
        euler[2] = point[2]

        quaternion = self._euler_to_quaternion(euler)
        person.pose.orientation.x = quaternion[0]
        person.pose.orientation.y = quaternion[1]
        person.pose.orientation.z = quaternion[2]
        person.pose.orientation.w = quaternion[3]
        self.pub_model.publish(person)


def shutdown():
    rospy.logwarn("Shutdown...")
    # noinspection PyProtectedMember
    os._exit(0)


def main():
    rospy.on_shutdown(shutdown)
    rospy.init_node('sc17cs_person_moving')

    __moving = PersonMoving()
    __moving.start()


if __name__ == '__main__':
    main()
