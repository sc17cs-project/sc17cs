#!/usr/bin/env python
import math
import threading
from numpy import ndarray
import cv2
import rospy
import time
from scan8 import Scan
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
from controllers.abase_controller import BaseController
from geometry_msgs.msg import Twist
import numpy as np
import tf
from enum import Enum, unique


@unique
class Task(Enum):
    ENTER = 0
    LEAVE = 1


class Move:
    GOAL_COLOR = (255, 191, 0)
    EVALUATE_THRESHOLD = 3
    PARKING_EXPAND = 0.10
    CORRECT_ANGLE_THRESHOLD = 15
    STAY_IN_RESTRICTED = 30

    def __init__(self):
        self.__scan = Scan()
        self.__base = None  # type: BaseController

        self.bridge = CvBridge()
        self.vel_publisher = None  # type: rospy.Publisher
        self.output_publisher = None  # type: rospy.Publisher

        self.parking_radius_pixels = 0
        self.goal_pixel = (0, 0)
        self.listener = None  # type: tf.TransformListener

        self.base_mask = None  # type: ndarray
        self.rgbd_mask = None  # type: ndarray

        self.pose_updated = False

        # For enter task
        self.enter_arrived_elevator = False
        self.enter_restricted_start = 0
        self.enter_stay_in_elevator = 0
        self.enter_back_to_waiting = False
        self.enter_go_to_elevator = False

        # For leave task
        self.leave_exit_elevator = False

        self.current_task = None  # type: Task

    def _init_tf(self):
        self.listener = tf.TransformListener()
        rospy.loginfo("Ready to start tf listener...")
        time.sleep(2)

    def init(self):
        tf_thread = threading.Thread(target=self._init_tf)
        tf_thread.daemon = True
        tf_thread.start()

        self.vel_publisher = rospy.Publisher('mobile_base_controller/cmd_vel', Twist, queue_size=10)
        self.output_publisher = rospy.Publisher('sc17cs/move/output', Image, queue_size=5)
        self.__base = BaseController()
        self.__base.reg_callback(self._reached_callback)

        elevator_data = rospy.wait_for_message("sc17cs/scan/elevator", Image, timeout=15)  # type: Image
        elevator_rgb = self.bridge.imgmsg_to_cv2(elevator_data, "bgr8")
        self.__scan.init_from_img(elevator_rgb)

        parking_expand_pixels = int(round(self.PARKING_EXPAND / self.__scan.resolution))
        self.parking_radius_pixels = self.__scan.robot_radius_pixels + parking_expand_pixels

        tf_thread.join()

        rospy.Subscriber("sc17cs/scan/output/map", Image, self._make_output)
        rospy.Subscriber("sc17cs/scan/mask", Image, self._make_mask)

        pose_thread = threading.Thread(target=self._update_pose)
        pose_thread.daemon = True
        pose_thread.start()

        while self.pose_updated is False or \
                self.base_mask is None or \
                self.rgbd_mask is None:
            time.sleep(1)

        # if self.current_task == self.TASK_LEAVE:
        #     self._correct_angle()
        #
        # start_thread = threading.Thread(target=self._start)
        # start_thread.daemon = True
        # start_thread.start()

    def _update_pose(self):
        """
        Update the position of the robot
        :return:
        """
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            (trans, quaternion) = self.listener.lookupTransform('/map', '/base_link', rospy.Time(0))
            x = round(trans[0], 2)
            y = round(trans[1], 2)
            euler = self._euler_from_quaternion(quaternion)
            yaw = round(euler[2], 2)
            robot_pose = (x, y, yaw)
            self.__scan.set_robot_pose(robot_pose)
            self.pose_updated = True
            # if self.current_task is None:
            #     if self.is_in_elevator():
            #         rospy.loginfo("Robot is in the elevator, the task is leaving")
            #         self.current_task = self.TASK_LEAVE
            #     else:
            #         rospy.loginfo("Robot is not in the elevator, the task is entering")
            #         self.current_task = self.TASK_ENTER
            rate.sleep()

    def _make_output(self, output_data):
        """
         Publish image as output to other modules
        :param output_data:
        :return:
        """
        output_image = self.bridge.imgmsg_to_cv2(output_data, "bgr8")

        # draw the parking pixel
        goal_col, goal_row = self.goal_pixel
        cv2.rectangle(output_image,
                      (goal_col - self.parking_radius_pixels, goal_row - self.parking_radius_pixels),
                      (goal_col + self.parking_radius_pixels, goal_row + self.parking_radius_pixels),
                      self.GOAL_COLOR, 1)
        cv2.circle(output_image, (goal_col, goal_row), self.__scan.robot_radius_pixels, self.GOAL_COLOR, -1)
        self.output_publisher.publish(self.bridge.cv2_to_imgmsg(output_image, "bgr8"))

    def _make_mask(self, mask_data):
        mask_image = self.bridge.imgmsg_to_cv2(mask_data, "mono8")  # type: ndarray
        height, width = mask_image.shape[:2]
        mid = int(width / 2)
        self.base_mask = mask_image[:, 0:mid]
        self.rgbd_mask = mask_image[:, mid: mid * 2]

    def enter(self, wait_cb=None):
        self.current_task = Task.ENTER

        self.enter_arrived_elevator = False
        self.enter_restricted_start = 0
        self.enter_stay_in_elevator = 0
        self.enter_back_to_waiting = False
        self.enter_go_to_elevator = False

        rate = rospy.Rate(3)
        while not rospy.is_shutdown():
            rospy.loginfo("Current pose (%.2f,%.2f,%.2f)", *self.__scan.robot_pose)
            rospy.loginfo("Current pixel (%d,%d)", *self.__scan.robot_pixel)
            if self.enter_arrived_elevator is True:
                rospy.loginfo("Successfully entered the elevator")
                break
            self._entering(wait_cb)
            rate.sleep()

    def leave(self):
        self.current_task = Task.LEAVE

        self.leave_exit_elevator = False

        rate = rospy.Rate(3)
        while not rospy.is_shutdown():
            rospy.loginfo("Current pose (%.2f,%.2f,%.2f)", *self.__scan.robot_pose)
            rospy.loginfo("Current pixel (%d,%d)", *self.__scan.robot_pixel)
            if self.leave_exit_elevator is True:
                rospy.loginfo("Successfully leaved the elevator!")
                break
            self._leaving()
            rate.sleep()

    def _leaving(self):
        if self.is_in_elevator() or self.is_in_restricted():
            rospy.loginfo("Robot in elevator or restricted area")
            rospy.loginfo("Try to find waiting pose")
            parking_pixel = self._find_parking(self.__scan.elevator_and_restricted_and_waiting_gray, self.rgbd_mask,
                                               self._eval_leave_elevator)
            if parking_pixel is not None:
                rospy.loginfo("Found waiting pose, go to there")
                self._go(parking_pixel, self.__scan.leave_degree)
                return
            else:
                rospy.loginfo("No waiting pose, stop")
                self._stop(self.__scan.leave_degree)
                return
                # rospy.loginfo("No leaving pose again, rotating for better view")
                # self._turn_left()
                # parking_pixel = self._find_parking(self.__scan.elevator_and_restricted_and_waiting_gray,
                #                                        self.rgbd_mask,
                #                                        self._eval_leave_elevator)
                # if parking_pixel is not None:
                #     rospy.loginfo("Found leaving pose in left, go to there")
                #     self.enter_go_to_elevator = True
                #     self._go(parking_pixel)
                #     return
                #
                # self._turn_right()
                # parking_pixel = self._find_parking(self.__scan.elevator_and_restricted_and_waiting_gray,
                #                                        self.rgbd_mask,
                #                                        self._eval_leave_elevator)
                # if parking_pixel is not None:
                #     rospy.loginfo("Found leaving pose in right, go to there")
                #     self.enter_go_to_elevator = True
                #     self._go(parking_pixel)
                #     return
                #
                # rospy.loginfo("No leaving pose and finish rotating, waiting...")
                # self._correct_angle()
                # return

        if self.is_in_waiting():
            self.leave_exit_elevator = True
            self._stop(self.__scan.leave_degree)
            return

    def _entering(self, wait_cb=None):

        if self.enter_back_to_waiting is True:
            if self.is_next_to_waiting():
                rospy.loginfo("Robot is next to waiting area, stop back to waiting procedure")
                self.enter_back_to_waiting = False
                self._stop(self.__scan.enter_degree)
                return

            rospy.loginfo("Robot is executing back to waiting procedure, try to find waiting pose")
            parking_pixel = self._find_parking(self.__scan.waiting_gray, self.rgbd_mask, self._eval_enter_waiting)
            if parking_pixel is not None:
                rospy.loginfo("Found waiting pose, go to there")
                self._go(parking_pixel, self.__scan.enter_degree)
                return
            else:
                rospy.loginfo("No waiting pose, try to use laser")
                parking_pixel = self._find_parking(self.__scan.waiting_gray, self.base_mask,
                                                   self._eval_enter_waiting)
                if parking_pixel is not None:
                    rospy.loginfo("Found waiting pose by using laser, go to there")
                    self._go(parking_pixel, self.__scan.enter_degree)
                    return
                rospy.loginfo("No waiting pose by using laser, go to default waiting pose")
                self._go(self.__scan.default_waiting_pixel, self.__scan.enter_degree)
                return

        if self.enter_go_to_elevator:
            if self.is_next_to_restricted():
                rospy.loginfo("Robot is next to restricted area and executing go to elevator procedure.")
                if self.enter_restricted_start == 0:
                    self.enter_restricted_start = int(time.time())

                passed_seconds = int(time.time()) - self.enter_restricted_start
                if passed_seconds > self.STAY_IN_RESTRICTED:
                    rospy.logwarn(
                        "Robot stays in restricted area more than %d seconds, turn to leaving angle",
                        self.STAY_IN_RESTRICTED)
                    self.enter_back_to_waiting = True
                    self.enter_go_to_elevator = False
                    self._stop(self.__scan.leave_degree)
                    return

                rospy.loginfo("Robot still can stay for %d seconds", self.STAY_IN_RESTRICTED - passed_seconds)

                rospy.loginfo("Try to find elevator pose in restricted and elevator area")
                parking_pixel = self._find_parking(self.__scan.restricted_and_elevator_gray, self.rgbd_mask,
                                                   self._eval_enter_elevator)
                if parking_pixel is None:
                    rospy.loginfo("No elevator pose in restricted and elevator area, stop")
                    self._stop(self.__scan.enter_degree)
                    return
                    # rospy.loginfo("No entering pose in restricted and elevator area, stop")
                    # self._stop()
                    # rospy.loginfo("Rotating for better view")
                    #
                    # self._turn_left()
                    # parking_pixel = self._find_parking(self.__scan.elevator_gray, self.rgbd_mask,
                    #                                        self._eval_enter_elevator)
                    # if parking_pixel is not None:
                    #     rospy.loginfo(
                    #         "Found entering pose of restricted and elevator area in left, go to there")
                    #     self.enter_restricted_start = int(time.time())
                    #     self._go(parking_pixel)
                    #     return
                    #
                    # self._turn_right()
                    # parking_pixel = self._find_parking(self.__scan.elevator_gray, self.rgbd_mask,
                    #                                        self._eval_enter_elevator)
                    # if parking_pixel is not None:
                    #     rospy.loginfo(
                    #         "Found entering pose of restricted and elevator area in right, go to there")
                    #     self.enter_restricted_start = int(time.time())
                    #     self._go(parking_pixel)
                    #     return
                    #
                    # rospy.loginfo("No entering pose in restricted and elevator area and finish rotating, waiting...")
                    # return
                else:
                    rospy.loginfo("New elevator pose in restricted and elevator area, go to there")
                    # self.enter_restricted_start = int(time.time())
                    self._go(parking_pixel, self.__scan.enter_degree)
                    return
            else:
                self.enter_restricted_start = 0

        # if self.is_in_restricted():
        #     rospy.loginfo("Robot in restricted area.")
        #
        #     rospy.loginfo("Try to find entering pose in waiting area")
        #     parking_pixel = self._find_parking(self.__scan.waiting_gray, self.rgbd_mask,
        #                                        self._eval_enter_waiting)
        #     if parking_pixel is not None:
        #         rospy.loginfo("Found waiting pose, go to there")
        #         self._go(parking_pixel, self.__scan.enter_degree)
        #         return
        #     else:
        #         rospy.loginfo("No entering pose for rgbd, try to use laser")
        #         parking_pixel = self._find_parking(self.__scan.waiting_gray, self.base_mask,
        #                                            self._eval_enter_waiting)
        #         if parking_pixel is not None:
        #             rospy.loginfo("Found waiting pose by using laser, go to there")
        #             self._go(parking_pixel, self.__scan.enter_degree)
        #             return
        #         rospy.loginfo("No entering pose, stop")
        #         self._stop(self.__scan.enter_degree)
        #         return

        if self.is_in_restricted():
            rospy.loginfo("Robot in restricted area")
            rospy.loginfo("Try to find waiting pose")
            parking_pixel = self._find_parking(self.__scan.waiting_gray, self.rgbd_mask,
                                               self._eval_enter_waiting)
            if parking_pixel is not None:
                rospy.loginfo("Found waiting pose, go to there")
                self._go(parking_pixel, self.__scan.enter_degree)
                return
            else:
                rospy.loginfo("No waiting pose, turn to leaving angle")
                self._stop(self.__scan.leave_degree)
                return

        if self.is_next_to_waiting():
            rospy.loginfo("Robot is next to waiting area")
            rospy.loginfo("Try to find waiting pose")
            parking_pixel = self._find_parking(self.__scan.waiting_gray, self.rgbd_mask,
                                               self._eval_enter_waiting)
            if parking_pixel is not None:
                rospy.loginfo("Found waiting pose, go to there")
                self._go(parking_pixel, self.__scan.enter_degree)
                return
            else:
                rospy.loginfo("No waiting pose, is the elevator door open?")
                elevator_pixel = self._find_parking(self.__scan.elevator_gray, self.base_mask,
                                                    self._eval_enter_elevator)
                if elevator_pixel is None:
                    rospy.loginfo("The door is closed, stop")
                    self._stop(self.__scan.enter_degree)
                    if callable(wait_cb):
                        wait_cb(True)
                    return
                else:
                    if self.enter_go_to_elevator is False:
                        rospy.loginfo("The laser thinks the door is open, stop")
                        self._stop(self.__scan.enter_degree)
                        self.enter_go_to_elevator = True
                        if callable(wait_cb):
                            wait_cb(False)
                        return
                    else:
                        rospy.loginfo("Try to find waiting pose in waiting and restricted area")
                        parking_pixel = self._find_parking(self.__scan.waiting_and_restricted_gray,
                                                           self.rgbd_mask,
                                                           self._eval_enter_waiting)
                        if parking_pixel is not None:
                            rospy.loginfo("Found waiting pose, go to there")
                            self._go(parking_pixel, self.__scan.enter_degree)
                            return

                        # rospy.loginfo("No entering pose again, rotating for better view")
                        # self._turn_left()
                        # parking_pixel = self._find_parking(self.__scan.elevator_and_restricted_and_waiting_gray,
                        #                                        self.rgbd_mask,
                        #                                        self._eval_enter_elevator)
                        # if parking_pixel is not None:
                        #     rospy.loginfo("Found entering pose in left, go to there")
                        #     self.enter_go_to_elevator = True
                        #     self._go(parking_pixel)
                        #     return
                        #
                        # self._turn_right()
                        # parking_pixel = self._find_parking(self.__scan.elevator_and_restricted_and_waiting_gray,
                        #                                        self.rgbd_mask,
                        #                                        self._eval_enter_elevator)
                        # if parking_pixel is not None:
                        #     rospy.loginfo("Found entering pose in right, go to there")
                        #     self.enter_go_to_elevator = True
                        #     self._go(parking_pixel)
                        #     return
                        #
                        # rospy.loginfo("No entering pose and finish rotating, waiting...")
                        # self._correct_angle()
                        else:
                            rospy.loginfo("No waiting pose, stop")
                            self._stop(self.__scan.enter_degree)
                            return

        if self.is_in_elevator():
            rospy.loginfo("Robot in elevator area")
            rospy.loginfo("Try to find elevator pose in elevator area")
            parking_pixel = self._find_parking(self.__scan.elevator_gray, self.rgbd_mask,
                                               self._eval_enter_elevator)
            if parking_pixel is not None:
                self.enter_stay_in_elevator = int(time.time())
                rospy.loginfo("Found elevator pose, go to there")
                self._go(parking_pixel, self.__scan.enter_degree)
                return
            else:
                if int(time.time()) - self.enter_stay_in_elevator > 5:
                    self.enter_go_to_elevator = False
                    rospy.loginfo("No elevator pose more than 5 seconds, stop and turn to leave angle")
                    self._stop(self.__scan.leave_degree)
                    self.enter_arrived_elevator = True
                    return
                else:
                    rospy.loginfo("No elevator pose, stop")
                    self._stop(self.__scan.enter_degree)
                    return

        rospy.loginfo("Robot in unknown area")
        rospy.loginfo("Try to find waiting pose in waiting area")
        parking_pixel = self._find_parking(self.__scan.waiting_gray, self.rgbd_mask, self._eval_enter_waiting)
        if parking_pixel is not None:
            rospy.loginfo("Found waiting pose, go to there")
            self._go(parking_pixel, self.__scan.enter_degree)
            return
        else:
            rospy.loginfo("No waiting pose, try to use laser")
            parking_pixel = self._find_parking(self.__scan.waiting_gray, self.base_mask,
                                               self._eval_enter_waiting)
            if parking_pixel is not None:
                rospy.loginfo("Found waiting pose by using laser, go to there")
                self._go(parking_pixel, self.__scan.enter_degree)
                return
            rospy.loginfo("No waiting pose by using laser, go to default waiting pose")
            self._go(self.__scan.default_waiting_pixel, self.__scan.enter_degree)
            return

    def _find_parking(self, area_gray, interest_area_mask, sort_function):
        robot_col, robot_row = self.__scan.robot_pixel
        area_mask = interest_area_mask.copy()
        area_mask[
        (robot_row - self.parking_radius_pixels): (robot_row + self.parking_radius_pixels + 1),
        (robot_col - self.parking_radius_pixels):(robot_col + self.parking_radius_pixels + 1)
        ] = 255

        free_gray = cv2.bitwise_and(area_gray, area_gray, mask=area_mask)
        free_pixels = self.__scan.search_pixels(free_gray != 0)
        if len(free_pixels) == 0:
            return None

        if self.current_task == Task.ENTER:
            free_pixels.sort(key=sort_function)
        else:
            free_pixels.sort(key=sort_function, reverse=True)

        current_score = sort_function(self.__scan.robot_pixel)

        for start_col, start_row in free_pixels:
            top = start_row - self.parking_radius_pixels
            bottom = start_row + self.parking_radius_pixels + 1
            left = start_col - self.parking_radius_pixels
            right = start_col + self.parking_radius_pixels + 1
            window = free_gray[top: bottom, left:right]
            if len(np.where(window == 0)[0]) == 0:
                if left <= robot_col <= right and top <= robot_row <= bottom:
                    return None
                target_score = sort_function((start_col, start_row))
                score_diff = abs(target_score - current_score)
                rospy.loginfo("New pose, score diff: %.4f", score_diff)
                if score_diff < self.EVALUATE_THRESHOLD:
                    return None
                return start_col, start_row

        return None

    def _reached_callback(self, state):
        pass

    def _eval_enter_elevator(self, pixel):
        x, y = pixel

        line_a, line_b, line_c = self.__scan.elevator_bottom_line
        bottom_distance = abs(line_a * x + line_b * y + line_c) / np.sqrt(line_a ** 2 + line_b ** 2)

        line_a, line_b, line_c = self.__scan.elevator_mid_line
        mid_distance = abs(line_a * x + line_b * y + line_c) / np.sqrt(line_a ** 2 + line_b ** 2)
        return 0.8 * bottom_distance + 0.2 * mid_distance

    def _eval_leave_elevator(self, pixel):
        x, y = pixel

        line_a, line_b, line_c = self.__scan.elevator_bottom_line
        bottom_distance = abs(line_a * x + line_b * y + line_c) / np.sqrt(line_a ** 2 + line_b ** 2)

        return bottom_distance

    def _eval_enter_waiting(self, pixel):
        x, y = pixel

        line_a, line_b, line_c = self.__scan.elevator_bottom_line
        bottom_distance = abs(line_a * x + line_b * y + line_c) / np.sqrt(line_a ** 2 + line_b ** 2)

        line_a, line_b, line_c = self.__scan.elevator_mid_line
        mid_distance = abs(line_a * x + line_b * y + line_c) / np.sqrt(line_a ** 2 + line_b ** 2)
        return 0.3 * bottom_distance + 0.7 * mid_distance

    def is_in_waiting(self):
        col, row = self.__scan.robot_pixel
        window = self.__scan.waiting_gray[
                 (row - self.parking_radius_pixels):(row + self.parking_radius_pixels + 1),
                 (col - self.parking_radius_pixels):(col + self.parking_radius_pixels + 1)
                 ]
        return len(np.where(window == 0)[0]) == 0

    def is_next_to_waiting(self):
        col, row = self.__scan.robot_pixel
        window = self.__scan.waiting_gray[
                 (row - self.parking_radius_pixels):(row + self.parking_radius_pixels + 1),
                 (col - self.parking_radius_pixels):(col + self.parking_radius_pixels + 1)
                 ]
        return len(np.where(window != 0)[0]) > 0

    def is_in_restricted(self):
        col, row = self.__scan.robot_pixel
        window = self.__scan.restricted_logical[
                 (row - self.parking_radius_pixels):(row + self.parking_radius_pixels + 1),
                 (col - self.parking_radius_pixels):(col + self.parking_radius_pixels + 1)
                 ]
        return len(np.where(np.equal(window, False))[0]) == 0

    def is_next_to_restricted(self):
        col, row = self.__scan.robot_pixel
        window = self.__scan.restricted_logical[
                 (row - self.parking_radius_pixels):(row + self.parking_radius_pixels + 1),
                 (col - self.parking_radius_pixels):(col + self.parking_radius_pixels + 1)
                 ]
        return len(np.where(np.equal(window, True))[0]) > 0

    def is_in_elevator(self):
        col, row = self.__scan.robot_pixel
        window = self.__scan.elevator_gray[
                 (row - self.parking_radius_pixels):(row + self.parking_radius_pixels + 1),
                 (col - self.parking_radius_pixels):(col + self.parking_radius_pixels + 1)
                 ]

        return len(np.where(window == 0)[0]) == 0

    @staticmethod
    def _euler_from_quaternion(quaternion):
        # noinspection PyUnresolvedReferences
        return tf.transformations.euler_from_quaternion(quaternion)  # type: list

    # def _turn_right(self):
    #     self._turn(-15)
    #
    # def _turn_left(self):
    #     self._turn(15)
    #
    # def _turn(self, speed):
    #     self._stop()
    #     angular_speed = math.radians(speed)
    #     relative_angle = math.radians(30)
    #     t0 = rospy.Time.now().to_sec()
    #     current_angle = 0
    #
    #     desired_velocity = Twist()
    #     desired_velocity.linear.x = 0
    #     desired_velocity.linear.y = 0
    #     desired_velocity.linear.z = 0
    #     desired_velocity.angular.x = 0
    #     desired_velocity.angular.y = 0
    #     desired_velocity.angular.z = angular_speed
    #
    #     while current_angle < relative_angle:
    #         self.vel_publisher.publish(desired_velocity)
    #         t1 = rospy.Time.now().to_sec()
    #         current_angle = abs(angular_speed) * (t1 - t0)

    def _go(self, pixel, degrees, force=False):
        if force is False and self.goal_pixel == pixel and self.__base.is_processing():
            return
        self.goal_pixel = pixel
        pose = self.__scan.pixel_to_coordinate(pixel)
        self.__base.goto(pose, degrees=degrees)

    def _reach(self, pixel, degrees):
        self.goal_pixel = pixel
        pose = self.__scan.pixel_to_coordinate(pixel)
        result = self.__base.reach_to(pose, degrees=degrees)
        return result

    def _stop(self, target_degrees):
        self.__base.cancel()
        min_speed = math.radians(5)
        max_speed = math.radians(50)
        rate = rospy.Rate(10)
        mercy_degrees = 3
        while not rospy.is_shutdown():
            (trans, quaternion) = self.listener.lookupTransform('/map', '/base_link', rospy.Time(0))
            euler = self._euler_from_quaternion(quaternion)
            yaw = round(euler[2], 2)
            yaw_degrees = math.degrees(yaw)
            diff_degrees = target_degrees - yaw_degrees
            if abs(diff_degrees) > mercy_degrees:

                diff_times = abs(int(math.floor(diff_degrees / mercy_degrees)))
                if diff_times > 1:
                    angular_speed = diff_times * min_speed * 0.8
                    if angular_speed > max_speed:
                        angular_speed = max_speed
                else:
                    angular_speed = min_speed

                desired_velocity = Twist()
                desired_velocity.linear.x = 0
                desired_velocity.linear.y = 0
                desired_velocity.linear.z = 0
                desired_velocity.angular.x = 0
                desired_velocity.angular.y = 0
                desired_velocity.angular.z = angular_speed if diff_degrees > 0 else angular_speed * -1
                self.vel_publisher.publish(desired_velocity)
                rospy.loginfo("Correcting, diff: %.2f, speed: %.2f degrees/s", diff_degrees,
                              math.degrees(angular_speed))
            else:
                rospy.loginfo("Stop, angle diff: %.2f", diff_degrees)
                return
            self.goal_pixel = self.__scan.robot_pixel
            rate.sleep()

    # def _correct_angle(self, degree):
    #     current_degree = math.degrees(self.__scan.robot_pose[2])
    #     diff = abs(current_degree - degree)
    #     if diff > self.CORRECT_ANGLE_THRESHOLD:
    #         rospy.loginfo("Angle diff: %.4f, correct angle...", diff)
    #         self._stop(degree)
