import json


class IntentMsg:
    GOAL_SET = 0
    GOAL_REACH = 1
    BUTTON_PRESSED = 2
    CONFIRM = 3
    REPEAT = 4
    NONE = 5

    def __init__(self, intent_type, data=None):
        self.data = data
        self.intent_type = intent_type

    def json(self):
        """
        Convert a message to a JSON string
        :return:
        """
        return json.dumps({"intent_type": self.intent_type, "data": self.data})

    @staticmethod
    def load(json_string):
        """
        Convert a string to an IntentMsg
        :param json_string:
        :return:
        """
        data = json.loads(json_string)
        if "intent_type" in data and "data" in data:
            intent_type = int(data['intent_type'])
            if IntentMsg.GOAL_SET <= intent_type <= IntentMsg.NONE:
                return IntentMsg(intent_type, data['data'])
        return IntentMsg(IntentMsg.NONE)

    def is_type(self, intent_type):
        return self.intent_type == intent_type
