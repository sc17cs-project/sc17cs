#!/usr/bin/env python

import rospy
import os
from geometry_msgs.msg import Twist
from gazebo_msgs.msg import ModelState, ModelStates
from geometry_msgs.msg import Point, Pose
import argparse
import sys


class Door:
    def __init__(self):
        self.step = 0.1
        self.pub_model = rospy.Publisher('gazebo/set_model_state', ModelState, queue_size=1)

    def open(self):
        """
        Open the elevator door
        :return:
        """
        print ("Open the door...")
        right_door_ready = False
        left_door_ready = False
        rate = rospy.Rate(10)
        while (not rospy.is_shutdown()) and (right_door_ready is False or left_door_ready is False):
            model = rospy.wait_for_message('gazebo/model_states', ModelStates)  # type: ModelStates
            for index in range(len(model.name)):
                name = model.name[index]
                pose = model.pose[index]  # type: Pose
                if name == "right_door" and right_door_ready is False:
                    # print("right door", pose.position.x)
                    if pose.position.x + self.step < 2.76:
                        obstacle = ModelState()
                        obstacle.model_name = name
                        obstacle.pose = model.pose[index]
                        obstacle.pose.position.x += self.step
                        self.pub_model.publish(obstacle)
                    else:
                        right_door_ready = True
                if name == "left_door" and left_door_ready is False:
                    # print("left door", pose.position.x)
                    if pose.position.x - self.step > -2.62:
                        obstacle = ModelState()
                        obstacle.model_name = name
                        obstacle.pose = model.pose[index]
                        obstacle.pose.position.x -= self.step
                        self.pub_model.publish(obstacle)
                    else:
                        left_door_ready = True
            rate.sleep()

    def close(self):
        """
        Close the elevator door
        :return:
        """
        print ("Close the door...")
        right_door_ready = False
        left_door_ready = False
        rate = rospy.Rate(10)
        while (not rospy.is_shutdown()) and (right_door_ready is False or left_door_ready is False):
            model = rospy.wait_for_message('gazebo/model_states', ModelStates)  # type: ModelStates
            for index in range(len(model.name)):
                name = model.name[index]
                pose = model.pose[index]  # type: Pose
                if name == "right_door" and right_door_ready is False:
                    # print("right door", pose.position.x)
                    if pose.position.x - self.step > 1.05:
                        obstacle = ModelState()
                        obstacle.model_name = name
                        obstacle.pose = model.pose[index]
                        obstacle.pose.position.x -= self.step
                        self.pub_model.publish(obstacle)
                    else:
                        right_door_ready = True

                if name == "left_door" and left_door_ready is False:
                    # print("left door", pose.position.x)
                    if pose.position.x + self.step < -1.00:
                        obstacle = ModelState()
                        obstacle.model_name = name
                        obstacle.pose = model.pose[index]
                        obstacle.pose.position.x += self.step
                        self.pub_model.publish(obstacle)
                    else:
                        left_door_ready = True
            rate.sleep()


def shutdown():
    # noinspection PyProtectedMember
    os._exit(0)


def main():
    ap = argparse.ArgumentParser()
    group = ap.add_mutually_exclusive_group(required=True)
    group.add_argument("--open", dest="open", help="Open the door", action='store_true')
    group.add_argument("--close", dest="open", help="Close the door", action='store_false')
    args = vars(ap.parse_args())
    rospy.on_shutdown(shutdown)
    rospy.init_node('door_control')
    __door = Door()
    if args['open'] is True:
        __door.open()
    else:
        __door.close()


if __name__ == '__main__':
    main()
