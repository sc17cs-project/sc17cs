#!/usr/bin/env python
from __future__ import print_function

import rospy
import time
import os
from geometry_msgs.msg import Point, Pose
from gazebo_msgs.msg import ModelState, ModelStates
import tf

__DIR__ = os.path.split(os.path.realpath(__file__))[0]


class RecordMoving:
    def __init__(self):
        self.model_name = ""
        self.save_path = os.path.join(__DIR__, "../", "records/")

    def start(self):
        model_name = self._input("Please input model name: ")
        if model_name != "":
            self.model_name = model_name
            self._record()

    @staticmethod
    def _input(string=None):
        if string is None:
            return str(raw_input()).strip()
        else:
            return str(raw_input(">>> " + string)).strip()

    @staticmethod
    def _output(string, *args, **kwargs):
        if string is None:
            string = ""
        print(">>> " + string, *args, **kwargs)

    @staticmethod
    def _euler_from_quaternion(quaternion):
        # noinspection PyUnresolvedReferences
        return tf.transformations.euler_from_quaternion(quaternion)  # type: list

    def _record(self):
        file_name = "%s_%d.txt" % (self.model_name, int(time.time()))
        file_path = os.path.join(self.save_path, file_name)
        with open(file_path, "w") as f:
            rate = rospy.Rate(10)
            while not rospy.is_shutdown():
                model = rospy.wait_for_message('gazebo/model_states', ModelStates)  # type: ModelStates
                for index in range(len(model.name)):
                    name = model.name[index]
                    if name == self.model_name:
                        pose = model.pose[index]  # type: Pose
                        position = pose.position
                        quaternion = pose.orientation
                        euler = self._euler_from_quaternion([quaternion.x, quaternion.y, quaternion.z, quaternion.w])
                        result = "%.3f,%.3f,%.3f" % (position.x, position.y, euler[2])
                        rospy.loginfo(result)
                        f.write(result + "\n")
                        f.flush()
                        break
                rate.sleep()


def shutdown():
    rospy.logwarn("Shutdown...")
    # noinspection PyProtectedMember
    os._exit(0)


def main():
    rospy.on_shutdown(shutdown)
    rospy.init_node('sc17cs_record_moving')

    __record = RecordMoving()
    __record.start()


if __name__ == '__main__':
    main()
