#!/usr/bin/env python

import math
import time
from numpy import ndarray

import cv2
import argparse
import numpy as np
import rospy
from geometry_msgs.msg import Point
import tf
import os
import threading
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import OccupancyGrid
import Queue
from parking_pose import ParkingPose
import std_msgs.msg as msg
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

__DIR__ = os.path.split(os.path.realpath(__file__))[0]


class Scan:

    def __init__(self, elevator_image, parking_expand=0.35, display=False):
        self.ELEVATOR_FREE_COLOR = (0, 255, 0)
        self.ELEVATOR_RESTRICTED_COLOR = (0, 0, 255)
        self.ELEVATOR_BOTTOM_COLOR = (255, 0, 0)
        self.WAIT_AREA_COLOR = (0, 255, 255)

        self.FREE_COLOR = (0, 200, 255)
        self.ROBOT_COLOR = (34, 102, 0)
        self.PARKING_COLOR = (255, 191, 0)
        self.RANGE_COLOR = (170, 255, 204)

        self.elevator_image_path = elevator_image
        self.resolution = 0.0
        self.robot_radius = 0.27
        self.robot_radius_pixels = None  # type: int
        self.parking_expand = parking_expand
        self.parking_expand_pixels = None  # type: int
        self.origin = None  # type: Point
        self.map_height = 0
        self.map_width = 0
        self.map_rgb = None  # type: ndarray

        self.elevator_image = None  # type: ndarray
        self.elevator_free_image = None  # type: ndarray
        self.waiting_area_image = None  # type: ndarray
        self.interest_area_logical = None  # type: ndarray
        self.rgbd_save_area_logical = None  # type: ndarray
        self.elevator_bottom_line = None  # type: tuple

        self.elevator_free_center_pixel = (0, 0)
        self.waiting_area_default_pixel = (0, 0)

        self.output_image = None  # type: ndarray
        self.listener = None
        self.robot_pose = (0.0, 0.0, 0)
        self.robot_pixel = (0, 0)
        self.watching_degree = 0.0

        self.display = display

        rospy.on_shutdown(self.shutdown)
        rospy.init_node('sc17cs_scan', anonymous=True)

        self.rgbd_save_records = {}

        self.bridge = CvBridge()
        self.parking_publisher = rospy.Publisher('sc17cs/parking', msg.String, queue_size=1)
        self.scan_image = rospy.Publisher('sc17cs/scan', Image, queue_size=5)

    @staticmethod
    def shutdown():
        rospy.logwarn("Shutdown...")
        # noinspection PyProtectedMember
        os._exit(0)

    def start(self):
        map_message = rospy.wait_for_message("/map", OccupancyGrid, 3)  # type: OccupancyGrid
        self.map_width = map_message.info.width
        self.map_height = map_message.info.height
        self.resolution = map_message.info.resolution
        self.origin = map_message.info.origin.position
        map_data = np.reshape(map_message.data, (self.map_width, self.map_height))
        map_rgb = np.zeros((self.map_height, self.map_width, 3), np.uint8)
        map_rgb.fill(205)
        for row in range(self.map_height):
            for col in range(self.map_width):
                probability = map_data[row, col]
                if probability == -1:
                    continue
                if probability > 0:
                    color = 0
                else:
                    color = 255
                # color = (1 - probability) * 255
                map_rgb[row, col] = (color, color, color)
        self.map_rgb = cv2.flip(map_rgb, 0)
        rospy.loginfo("Get the map successfully, (%dx%d)", self.map_width, self.map_height)

        self.robot_radius_pixels = int(round(self.robot_radius / self.resolution))
        self.parking_expand_pixels = int(round(self.parking_expand / self.resolution))
        rospy.loginfo("Robot radius %d pixels. Parking expand %d pixels", self.robot_radius_pixels,
                      self.parking_expand_pixels)

        self.elevator_image = cv2.imread(self.elevator_image_path)
        # replace all 254 to 255
        self.elevator_image[np.where((self.elevator_image >= (254, 254, 254)).all(axis=2))] = (255, 255, 255)

        elevator_free_mask = cv2.inRange(self.elevator_image, self.ELEVATOR_FREE_COLOR, self.ELEVATOR_FREE_COLOR)
        self.elevator_free_image = cv2.bitwise_and(self.elevator_image, self.elevator_image, mask=elevator_free_mask)
        elevator_free_pixels = self.search_pixels(elevator_free_mask == 255)
        self.elevator_free_center_pixel = tuple(
            [int(round(n)) for n in np.mean(np.array(elevator_free_pixels), axis=0).tolist()])
        rospy.loginfo("Elevator free center pixel (%d,%d)", *self.elevator_free_center_pixel)

        waiting_area_mask = cv2.inRange(self.elevator_image, self.WAIT_AREA_COLOR, self.WAIT_AREA_COLOR)
        self.waiting_area_image = cv2.bitwise_and(self.elevator_image, self.elevator_image, mask=waiting_area_mask)
        waiting_area_pixels = self.search_pixels(waiting_area_mask == 255)
        self.waiting_area_default_pixel = tuple([int(round(n)) for n in
                                                 np.mean(np.array(waiting_area_pixels), axis=0).tolist()])
        rospy.loginfo("Waiting area default pixel (%d,%d)", *self.waiting_area_default_pixel)

        restricted_area_logical = (self.elevator_image == self.ELEVATOR_RESTRICTED_COLOR).all(axis=2)
        elevator_free_logical = (self.elevator_image == self.ELEVATOR_FREE_COLOR).all(axis=2)
        wait_area_logical = (self.elevator_image == self.WAIT_AREA_COLOR).all(axis=2)
        self.rgbd_save_area_logical = np.logical_or(elevator_free_logical, wait_area_logical)
        self.interest_area_logical = np.logical_or(restricted_area_logical, self.rgbd_save_area_logical)

        self.calculate_elevator_bottom()

        self.listener = tf.TransformListener()
        rospy.loginfo("Ready to start tf listener...")
        time.sleep(3)

        update_thread = threading.Thread(target=self.update)
        update_thread.daemon = True
        update_thread.start()

        if self.display is True:
            show_thread = threading.Thread(target=self.show)
            show_thread.daemon = True
            show_thread.start()

    def calculate_elevator_bottom(self):
        """
        Calculate the bottom line of the elevator area
        :return:
        """
        bottom_pixels = np.array(self.search_pixels((self.elevator_image == self.ELEVATOR_BOTTOM_COLOR).all(axis=2)))
        elevator_bottom_pixel = tuple([int(round(n)) for n in np.mean(np.array(bottom_pixels), axis=0).tolist()])

        # Fit the inner boundary into a straight line
        vx, vy, cx, cy = cv2.fitLine(bottom_pixels, cv2.DIST_L2, 0, 0.01, 0.01)

        # Should big enough
        m = 200

        # line_start/line_end = (x,y) => (col,row)
        line_start = (float(cx - m * vx[0]), float(cy - m * vy[0]))
        line_end = (float(cx + m * vx[0]), float(cy + m * vy[0]))

        # Fitting line [ line_a*x+ line_b*y + line_c = 0 ]
        line_a = line_end[1] - line_start[1]
        line_b = -1 * (line_end[0] - line_start[0]) + 1e-8
        line_c = -1 * line_a * elevator_bottom_pixel[0] - line_b * elevator_bottom_pixel[1]
        self.elevator_bottom_line = (line_a, line_b, line_c)
        line_slope = -1 * line_a / line_b
        line_slope_degree = math.degrees(math.atan(line_slope))
        rospy.loginfo("Bottom line: %.4f*x + %.4f*y + %.4f = 0; Slope: %.4f, Slope degree: %.4f", line_a, line_b,
                      line_c, line_slope, line_slope_degree)

        if line_slope_degree == 0:
            watching_degree = 90.0
        elif line_slope_degree < 0:
            if self.elevator_free_center_pixel[1] < elevator_bottom_pixel[1]:
                watching_degree = -1 * (90 + line_slope_degree)
            else:
                watching_degree = 90 - line_slope_degree
        else:
            if self.elevator_free_center_pixel[1] < elevator_bottom_pixel[1]:
                watching_degree = -1 * (90 + line_slope_degree)
            else:
                watching_degree = 90 - line_slope_degree
        self.watching_degree = round(watching_degree, 2)
        rospy.loginfo("Pose degree %.2f", self.watching_degree)

    def distance_to_elevator_bottom(self, pixel):
        x, y = pixel
        line_a, line_b, line_c = self.elevator_bottom_line
        return abs(line_a * x + line_b * y + line_c) / np.sqrt(line_a ** 2 + line_b ** 2)

    def update(self):
        while True:
            try:
                start_time = time.time()

                (trans, quaternion) = self.listener.lookupTransform('/map', '/base_link', rospy.Time(0))
                x = trans[0]
                y = trans[1]
                euler = self.euler_from_quaternion(quaternion)
                yaw = euler[2]
                self.robot_pose = (x, y, yaw)
                self.robot_pixel = tuple(self.coordinate_to_pixel(self.robot_pose))

                interest_area_mask, output_image = self.generate_scan()

                layers = [
                    {
                        "priority": ParkingPose.ELEVATOR,
                        "image": self.elevator_free_image
                    },
                    {
                        "priority": ParkingPose.WAITING,
                        "image": self.waiting_area_image
                    }
                ]

                # Waiting area default pose and pixel
                parking_pixel = self.waiting_area_default_pixel
                parking_pose = self.pixel_to_coordinate(parking_pixel, degree=True)
                parking_msg = ParkingPose(ParkingPose.DEFAULT, parking_pose, parking_pixel)

                for layer in layers:
                    parking_priority = layer['priority']
                    parking_pixel = self.generate_parking(layer['image'], interest_area_mask,
                                                          output_image)
                    if parking_pixel is not None:
                        parking_pose = self.pixel_to_coordinate(parking_pixel, degree=True)
                        parking_msg = ParkingPose(parking_priority, parking_pose, parking_pixel)
                        break

                end_time = time.time()
                rospy.loginfo("%.4fs. Parking: [%s].", end_time - start_time, parking_msg)
                self.parking_publisher.publish(parking_msg.json())

                # draw the pixel of robot
                cv2.circle(output_image, self.robot_pixel, self.robot_radius_pixels,
                           self.ROBOT_COLOR,
                           -1)
                output_msg = self.bridge.cv2_to_imgmsg(output_image, "bgr8")
                self.scan_image.publish(output_msg)
                self.output_image = output_image
            except rospy.ROSException:
                self.shutdown()

    def pixel_to_coordinate(self, col, row=None, degree=None):
        """
        :type degree: None|Bool|float
        :type col: list|Tuple|int
        :type row: None|int
        """
        if hasattr(col, '__len__') and len(col) >= 2 and row is None:
            row = col[1]
            col = col[0]
        result = [col * self.resolution + self.origin.x, (self.map_height - row) * self.resolution + self.origin.y]
        if degree is not None:
            if degree is True:
                result.append(self.watching_degree)
            else:
                result.append(float(degree))
        return tuple(result)

    def coordinate_to_pixel(self, x, y=None):
        """
        :type x: list|Tuple|float
        :type y: None|float
        """
        if hasattr(x, '__len__') and len(x) >= 2 and y is None:
            y = x[1]
            x = x[0]
        return int(round((x - self.origin.x) / self.resolution)), self.map_height - int(
            round(((y - self.origin.y) / self.resolution)))

    @staticmethod
    def euler_from_quaternion(quaternion):
        # noinspection PyUnresolvedReferences
        return tf.transformations.euler_from_quaternion(quaternion)  # type: list

    def get_rgbd_detected(self, queue):
        """
        Get the data obtained from the RGBD camera
        :param queue:
        :return:
        """
        rgbd_detected = []
        (trans_laser, quaternion_laser) = self.listener.lookupTransform('/map', '/rgbd_laser_link', rospy.Time(0))
        laser_message = rospy.wait_for_message("/rgbd_scan", LaserScan, 1)  # type: LaserScan
        distances = laser_message.ranges
        angle_min = laser_message.angle_min
        angle_increment = laser_message.angle_increment

        robot_pose_vector = np.array(self.robot_pose[:2]).reshape((2, 1))

        current_records = {}
        for index in range(len(distances)):
            distance = distances[index]
            angle = angle_min + angle_increment * index + self.robot_pose[2]

            dict_key = str(round(angle, 2))
            if not (math.isnan(distance) and dict_key in self.rgbd_save_records):
                self.rgbd_save_records[dict_key] = (distance, angle, None, None)
                current_records[dict_key] = True

            if math.isinf(distance) or math.isnan(distance):
                continue

            relative_x = np.cos(angle) * distance
            relative_y = np.sin(angle) * distance

            x = trans_laser[0] + relative_x
            y = trans_laser[1] + relative_y

            pixel = tuple(self.coordinate_to_pixel(x, y))
            if bool(self.rgbd_save_area_logical[pixel[1], pixel[0]]) is False:
                self.rgbd_save_records.pop(dict_key, None)
            else:
                self.rgbd_save_records[dict_key] = (distance, angle, x, y)

            absolute_distance = np.linalg.norm(robot_pose_vector - np.array((x, y)).reshape((2, 1)))

            rgbd_detected.append((round(angle, 2), None, absolute_distance, pixel))

        diff_keys = set(self.rgbd_save_records.keys()) - set(current_records.keys())
        for diff_key in diff_keys:
            payload = self.rgbd_save_records[diff_key]
            distance = payload[0]
            angle = payload[1]
            if math.isinf(distance) or math.isnan(distance):
                continue

            x = payload[2]
            y = payload[3]
            pixel = tuple(self.coordinate_to_pixel(x, y))

            absolute_distance = np.linalg.norm(robot_pose_vector - np.array((x, y)).reshape((2, 1)))
            rgbd_detected.append((round(angle, 2), None, absolute_distance, pixel))

        queue.put(rgbd_detected)

    def get_laser_scan(self, queue):
        (trans_laser, quaternion_laser) = self.listener.lookupTransform("/map", "/base_laser_link", rospy.Time(0))
        laser_message = rospy.wait_for_message("/scan", LaserScan, 1)  # type: LaserScan
        queue.put((trans_laser, laser_message))

    def generate_scan(self):
        output_image = self.map_rgb.copy()

        laser_queue = Queue.Queue()
        rgbd_queue = Queue.Queue()
        laser_thread = threading.Thread(target=self.get_laser_scan, args=(laser_queue,))
        laser_thread.daemon = True
        laser_thread.start()

        rgbd_thread = threading.Thread(target=self.get_rgbd_detected, args=(rgbd_queue,))
        rgbd_thread.daemon = True
        rgbd_thread.start()

        laser_thread.join()
        rgbd_thread.join()

        laser_data = laser_queue.get()
        trans_laser = laser_data[0]  # type: tuple
        laser_message = laser_data[1]  # type: LaserScan
        rgbd_detected = rgbd_queue.get()  # type: list

        poly_pixels = []
        distances = laser_message.ranges
        angle_min = laser_message.angle_min
        angle_increment = laser_message.angle_increment
        previous_pixel = None  # type: tuple

        poly_pixels.append(self.robot_pixel)
        rgbd_pixels = []
        rgbd_start = 0
        robot_pose_vector = np.array(self.robot_pose[:2]).reshape((2, 1))
        for index in range(len(distances)):
            distance = distances[index]
            if math.isinf(distance) or math.isnan(distance):
                continue

            o_angle = angle_min + angle_increment * index
            angle = o_angle + self.robot_pose[2]

            relative_x = np.cos(angle) * distance
            relative_y = np.sin(angle) * distance

            x = trans_laser[0] + relative_x
            y = trans_laser[1] + relative_y

            absolute_distance = None  # type: float
            pixel = None
            from_rgb = False
            for rgbd_index in range(rgbd_start, len(rgbd_detected)):
                rgbd_record = rgbd_detected[rgbd_index]  # type: tuple
                rgbd_angle = rgbd_record[0]  # type: float
                # rgbd_end_angle = rgbd_record[1]  # type: float
                rgbd_distance = rgbd_record[2]  # type: float
                rgbd_pixel = rgbd_record[3]  # type: tuple
                if rgbd_angle == round(angle, 2):
                    if absolute_distance is None:
                        absolute_distance = np.linalg.norm(robot_pose_vector - np.array((x, y)).reshape((2, 1)))
                    if rgbd_distance < absolute_distance or True:
                        pixel = rgbd_pixel
                        from_rgb = True
                        # rgbd_start = rgbd_index
                        break
            if pixel is None:
                pixel = tuple(self.coordinate_to_pixel(x, y))

            if pixel == previous_pixel:
                continue
            previous_pixel = pixel
            poly_pixels.append(pixel)
            if from_rgb is True:
                rgbd_pixels.append(pixel)

        poly_pixels.append(self.robot_pixel)

        laser_mask = np.full((self.map_height, self.map_width), 255, np.uint8)
        poly = np.array(poly_pixels, dtype=np.int32)
        poly = poly.reshape((-1, 1, 2))
        cv2.fillPoly(laser_mask, [poly], True, 0)
        # kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (2, 2))
        # laser_mask = cv2.dilate(laser_mask, kernel, iterations=1)

        # replace all <255 to 0
        laser_mask[laser_mask < 255] = 0
        laser_mask = 255 - laser_mask

        interest_area_mask = np.full((self.map_height, self.map_width), 0, np.uint8)
        interest_area_mask[np.logical_and(laser_mask == 255, self.interest_area_logical)] = 255
        output_image[interest_area_mask == 255] = self.FREE_COLOR
        for rgbd_pixel in rgbd_pixels:
            col, row = rgbd_pixel
            output_image[row, col] = (0, 0, 255)
        output_image[
            np.logical_and(laser_mask == 255, (output_image == (255, 255, 255)).all(axis=2))] = self.RANGE_COLOR

        return interest_area_mask, output_image

    def generate_parking(self, area_image, interest_area_mask, output_image):
        """
        Generate Parking Spot
        :param interest_area_mask:
        :param area_image:
        :param output_image:
        :return:
        """
        parking_radius_pixels = self.robot_radius_pixels + self.parking_expand_pixels

        cv2.rectangle(interest_area_mask,
                      (self.robot_pixel[0] - parking_radius_pixels, self.robot_pixel[1] - parking_radius_pixels),
                      (self.robot_pixel[0] + parking_radius_pixels, self.robot_pixel[1] + parking_radius_pixels),
                      255, -1)

        gray_area_image = cv2.cvtColor(area_image, cv2.COLOR_BGR2GRAY)
        free_area_image = cv2.bitwise_and(gray_area_image, gray_area_image, mask=interest_area_mask)

        free_pixels = self.search_pixels(free_area_image != 0)
        if len(free_pixels) == 0:
            return None
        free_pixels.sort(key=self.distance_to_elevator_bottom)

        for start_col, start_row in free_pixels:
            window = free_area_image[
                     (start_row - parking_radius_pixels): (start_row + parking_radius_pixels + 1),
                     (start_col - parking_radius_pixels):(start_col + parking_radius_pixels + 1)
                     ]
            if len(np.where(window == 0)[0]) == 0:
                cv2.rectangle(output_image, (start_col - parking_radius_pixels, start_row - parking_radius_pixels),
                              (start_col + parking_radius_pixels, start_row + parking_radius_pixels),
                              self.PARKING_COLOR, 1)
                cv2.circle(output_image, (start_col, start_row), self.robot_radius_pixels,
                           self.PARKING_COLOR, -1)
                return start_col, start_row

        start_col, start_row = self.robot_pixel
        if gray_area_image[start_row, start_col] != 0:
            cv2.rectangle(output_image, (start_col - parking_radius_pixels, start_row - parking_radius_pixels),
                          (start_col + parking_radius_pixels, start_row + parking_radius_pixels),
                          self.PARKING_COLOR, 1)
            cv2.circle(output_image, (start_col, start_row), self.robot_radius_pixels,
                       self.PARKING_COLOR, -1)
            return start_col, start_row
        else:
            return None

    @staticmethod
    def search_pixels(condition):
        """
        Search pixel
        :param condition:
        :return:
        """
        pixels = []
        pixels_search = np.where(condition)
        for index in range(len(pixels_search[0])):
            pixels.append((pixels_search[1][index], pixels_search[0][index]))
        return pixels

    @staticmethod
    def rgb_to_hsv(rgb_color):
        rgb_image = np.uint8([[rgb_color]])
        hsv_image = cv2.cvtColor(rgb_image, cv2.COLOR_BGR2HSV)
        return hsv_image[0, 0]

    def show(self):
        while True:
            if self.output_image is not None:
                cv2.namedWindow("scan", cv2.WINDOW_NORMAL)
                cv2.imshow("scan", self.output_image)
                cv2.waitKey(50)
            else:
                time.sleep(1)
        pass


def main():
    ap = argparse.ArgumentParser()
    # noinspection PyTypeChecker
    ap.add_argument("--elevator", type=str, help="The path of elevator.png", default='../maps/dev/elevator.png')

    # noinspection PyTypeChecker
    ap.add_argument("--parking_expand", type=float, help="The path of elevator.png", default=None)

    # noinspection PyTypeChecker
    ap.add_argument("--display", type=int, help="If set 1, display the scan image", default=1)
    args = vars(ap.parse_args())
    args = dict((k, v) for k, v in args.iteritems() if v is not None)

    params = {'elevator_image': os.path.abspath(os.path.join(__DIR__, os.path.expandvars(args["elevator"])))}
    if "parking_expand" in args:
        params['parking_expand'] = float(args['parking_expand'])
    if "display" in args:
        params['display'] = int(args['display']) == 1
    print("params", params)

    __scan = Scan(**params)
    __scan.start()

    # response the ctrl+c
    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()
