#!/usr/bin/env python

import curses
import math
import time

import rospy
from geometry_msgs.msg import Point, Pose
from gazebo_msgs.msg import ModelState, ModelStates
import tf


class PersonControl:
    def __init__(self, stdscr):
        self.pub_model = rospy.Publisher('gazebo/set_model_state', ModelState, queue_size=1)
        self.actions = {
            curses.KEY_UP: "Forward",
            curses.KEY_DOWN: "Backward",
            curses.KEY_LEFT: "Left",
            curses.KEY_RIGHT: "Right"
        }
        self.speed = 0.05
        self.throttle = 350
        self.last_moving = -1
        self.current_index = -1
        self.current_action = -1

        curses.curs_set(0)
        self.stdscr = stdscr
        self.stdscr.nodelay(True)

        self.display()

    @staticmethod
    def time():
        return int(round(time.time() * 1000))

    @staticmethod
    def _euler_from_quaternion(quaternion):
        # noinspection PyUnresolvedReferences
        return tf.transformations.euler_from_quaternion(quaternion)  # type: list

    @staticmethod
    def _euler_to_quaternion(euler):
        # noinspection PyUnresolvedReferences
        return tf.transformations.quaternion_from_euler(euler[0], euler[1], euler[2])  # type: list

    def display(self):
        """
        Display window
        :return:
        """
        self.stdscr.clear()

        if self.current_index == -1:
            self.stdscr.addstr("Current target: None \n")
        else:
            self.stdscr.addstr("Current target: person_moving_%d \n" % self.current_index)

        action_string = "None" if self.current_action == -1 else self.actions[self.current_action]
        self.stdscr.addstr("Last action: %s \n" % action_string)
        self.stdscr.addstr("Last moving at: %d\n" % self.last_moving)
        self.stdscr.addstr("=====================================\n")
        self.stdscr.addstr("Press 0-9 to select target.\n")
        self.stdscr.addstr("Press Ctrl+C to quit.\n")

    def run(self):
        action_keys = self.actions.keys()
        while not rospy.is_shutdown():
            while True:
                try:
                    key_code = self.stdscr.getch()
                    if key_code == -1:
                        break
                    else:
                        if 48 <= key_code <= 57:
                            self.current_index = int(chr(key_code))
                        elif key_code in action_keys:
                            if self.current_index != -1:
                                self.current_action = key_code
                                self.move()
                    self.display()
                except KeyboardInterrupt:
                    rospy.signal_shutdown('Bye')
                    break

    def move(self):
        if self.last_moving == -1:
            self.last_moving = self.time()
        elif self.time() - self.last_moving <= self.throttle:
            return
        self.last_moving = self.time()

        target_name = "person_moving_%d" % self.current_index
        model = rospy.wait_for_message('gazebo/model_states', ModelStates)  # type: ModelStates
        for index in range(len(model.name)):
            name = model.name[index]
            if name == target_name:
                pose = model.pose[index]  # type: Pose
                quaternion = pose.orientation
                x = pose.position.x
                y = pose.position.y
                euler = self._euler_from_quaternion([quaternion.x, quaternion.y, quaternion.z, quaternion.w])
                yaw = euler[2]

                new_x = x
                new_y = y
                if self.current_action == curses.KEY_UP:
                    new_y = y + self.speed
                elif self.current_action == curses.KEY_DOWN:
                    new_y = y - self.speed
                elif self.current_action == curses.KEY_LEFT:
                    new_x = x - self.speed
                elif self.current_action == curses.KEY_RIGHT:
                    new_x = x + self.speed

                person = ModelState()
                person.model_name = name
                person.pose = pose
                person.pose.position.x = new_x
                person.pose.position.y = new_y
                quaternion = self._euler_to_quaternion(euler)
                person.pose.orientation.x = 0.0
                person.pose.orientation.y = 0.0
                person.pose.orientation.z = quaternion[2]
                person.pose.orientation.w = quaternion[3]
                # rospy.loginfo("%s: (%.2f,%.2f),(%.2f,%.2f,%.2f)", name, x, y, new_x, new_y, yaw)
                self.pub_model.publish(person)
                break


def main(stdscr):
    rospy.init_node('sc17cs_person_control', anonymous=True)

    __control = PersonControl(stdscr)
    __control.run()


if __name__ == '__main__':
    curses.wrapper(main)
