#!/usr/bin/env python
import threading

import tf
import time
import rospy
import os


def shutdown():
    rospy.logwarn("Shutdown...")
    # noinspection PyProtectedMember
    os._exit(0)


def broadcast():
    listener = tf.TransformListener()
    time.sleep(2)
    broadcaster = tf.broadcaster.TransformBroadcaster()
    rospy.loginfo("TF broadcaster is ready")
    rate = rospy.Rate(20)
    first_time = True
    while not rospy.is_shutdown():
        (trans_depth, quaternion_laser) = listener.lookupTransform('/base_link', '/xtion_depth_frame', rospy.Time(0))
        broadcaster.sendTransform((0, 0, 0), quaternion_laser, rospy.Time.now(), "rgbd_depth_link",
                                  "base_link")
        if first_time is True:
            first_time = False
            rospy.loginfo("Start broadcast...")
        rate.sleep()


def main():
    rospy.on_shutdown(shutdown)
    rospy.init_node('rgbd_depth_link', anonymous=True)

    t1 = threading.Thread(target=broadcast)
    t1.daemon = True
    t1.start()
    while not rospy.is_shutdown():
        time.sleep(1)


if __name__ == '__main__':
    main()
