#!/usr/bin/env python
import threading
import time
import tempfile
import subprocess
import requests
import os
import Queue
import rospy
from requests import RequestException

__DIR__ = os.path.split(os.path.realpath(__file__))[0]

"""
1. Install vlc
sudo apt-get update
sudo apt-get install vlc
"""


class Speaker:

    def __init__(self, endpoint="http://127.0.0.1:1221"):
        self.endpoint = endpoint
        self.files = Queue.Queue()
        self.ding_path = os.path.join(__DIR__, "../", "resources/speaker_begin.mp3")

        self._test()
        rospy.loginfo("TTS Connected successfully")

        # Run the player method on another thread to prevent the main thread from being blocked.
        self.t1 = threading.Thread(target=self.player)
        self.t1.daemon = True
        self.t1.start()

    def player(self):
        while not rospy.is_shutdown():
            temp_file = self.files.get()
            with open(os.devnull, 'w') as null:
                subprocess.call("cvlc --play-and-exit %s" % temp_file, shell=True, stdout=null, stderr=null)

    def _test(self):
        try:
            response = requests.get(self.endpoint + "/ping", timeout=5)
            response.raise_for_status()
        except RequestException as e:
            rospy.logerr("TTS request error: %s", e.message)
            shutdown()

    def run(self, text):
        try:
            response = requests.post(self.endpoint + "/tts", data={
                'text': text
            }, timeout=5)
            if response.status_code == 200:
                temp = tempfile.mktemp() + ".mp3"
                with open(temp, 'wb') as f:
                    f.write(response.content)
                self.files.put(self.ding_path)
                self.files.put(temp)
            response.raise_for_status()
        except RequestException as e:
            rospy.logerr("TTS request error: %s", e.message)
            shutdown()


def shutdown():
    rospy.logwarn("Shutdown...")
    # noinspection PyProtectedMember
    os._exit(0)


def main():
    rospy.on_shutdown(shutdown)
    rospy.init_node('sc17cs_speaker', anonymous=True)

    s = Speaker()
    s.run("Life is full of confusing and disordering particular time, a particular location")
    s.run("The goal is 8th floor, right?")
    s.run("Could you help me press the button of the 6th floor, thank you.")
    s.run("Ok, I will continue to wait for the command for leaving the elevator.")
    s.run("Goal is reached, executing leaving elevator procedure.")

    # response the ctrl+c
    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()
