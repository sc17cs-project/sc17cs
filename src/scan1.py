#!/usr/bin/env python

import importlib
import math
import sys
import time
from numpy import ndarray

import cv2
import argparse
import numpy as np
import rospy
from geometry_msgs.msg import Point
import tf
import os
import threading
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import OccupancyGrid
import Queue

__DIR__ = os.path.split(os.path.realpath(__file__))[0]


class Scan:

    def __init__(self, elevator_image_path):
        self.ELEVATOR_AREA_COLOR = (0, 255, 0)
        self.ELEVATOR_EMPTY_COLOR = (255, 255, 255)
        self.OBSTACLE_COLOR = (0, 200, 255)
        self.DETECTED_COLOR = (0, 0, 255)
        self.ROBOT_COLOR = (34, 102, 0)
        self.PARKING_COLOR = (255, 191, 0)
        self.RANGE_COLOR = (170, 255, 204)
        self.WAIT_AREA_COLOR = (0, 255, 255)

        self.elevator_image_path = elevator_image_path
        self.resolution = 0.0
        self.robot_radius = 0.27
        self.robot_radius_pixels = 4
        self.parking_extra_pixels = 2
        self.origin = None  # type: Point
        self.map_height = 0
        self.map_width = 0
        self.map_rgb = None  # type: ndarray

        self.elevator_image = None  # type: ndarray
        self.elevator_hsv_image = None  # type: ndarray
        self.elevator_area_image = None  # type: ndarray
        self.waiting_area_image = None  # type: ndarray
        self.obstacle_area_logical = None  # type: ndarray
        self.elevator_bottom_vector = None  # type: ndarray

        self.output_image = None  # type: ndarray
        self.listener = None
        self.robot_pose = (0.0, 0.0, 0)
        self.laser_range = 0.0
        self.laser_radius = 0.0

        rospy.on_shutdown(self.shutdown)
        rospy.init_node('init1', anonymous=True)

    @staticmethod
    def shutdown():
        rospy.logwarn("Shutdown...")
        # noinspection PyProtectedMember
        os._exit(0)

    def start(self):
        self.set_laser_range()
        self.load_map()

    def set_laser_range(self):
        laser_message = rospy.wait_for_message("/scan", LaserScan, 1)  # type: LaserScan
        self.laser_range = abs(laser_message.angle_max - laser_message.angle_min)
        self.laser_radius = laser_message.range_max

    def load_map(self):
        """
        Load address
        :return:
        """
        map_message = rospy.wait_for_message("/map", OccupancyGrid, 3)  # type: OccupancyGrid
        self.map_width = map_message.info.width
        self.map_height = map_message.info.height
        self.resolution = map_message.info.resolution
        self.origin = map_message.info.origin.position
        map_data = np.reshape(map_message.data, (self.map_width, self.map_height))
        map_rgb = np.zeros((self.map_height, self.map_width, 3), np.uint8)
        map_rgb.fill(205)
        for row in range(self.map_height):
            for col in range(self.map_width):
                probability = map_data[row, col]
                if probability == -1:
                    continue
                if probability > 0:
                    color = 0
                else:
                    color = 255
                # color = (1 - probability) * 255
                map_rgb[row, col] = (color, color, color)
        self.map_rgb = cv2.flip(map_rgb, 0)
        rospy.loginfo("Get the map successfully, (%d x %d)", self.map_width, self.map_height)

        self.robot_radius_pixels = int(round(self.robot_radius / self.resolution))

        self.elevator_image = cv2.imread(self.elevator_image_path)
        # replace all 254 to 255
        self.elevator_image[np.where((self.elevator_image >= (254, 254, 254)).all(axis=2))] = (255, 255, 255)

        self.elevator_hsv_image = cv2.cvtColor(self.elevator_image, cv2.COLOR_BGR2HSV)
        hsv_elevator = self.rgb_to_hsv(self.ELEVATOR_AREA_COLOR)
        elevator_area_mask = cv2.inRange(self.elevator_hsv_image, hsv_elevator, hsv_elevator)
        self.elevator_area_image = cv2.bitwise_and(self.elevator_image, self.elevator_image, mask=elevator_area_mask)

        hsv_waiting = self.rgb_to_hsv(self.WAIT_AREA_COLOR)
        waiting_area_mask = cv2.inRange(self.elevator_hsv_image, hsv_waiting, hsv_waiting)
        self.waiting_area_image = cv2.bitwise_and(self.elevator_image, self.elevator_image, mask=waiting_area_mask)

        elevator_area_logical = (self.elevator_image == self.ELEVATOR_AREA_COLOR).all(axis=2)
        wait_area_logical = (self.elevator_image == self.WAIT_AREA_COLOR).all(axis=2)
        self.obstacle_area_logical = np.logical_or(elevator_area_logical, wait_area_logical)

        bottom_pixels = np.array(self.search_pixels((self.elevator_image == (0, 0, 255)).all(axis=2)))
        self.elevator_bottom_vector = np.mean(bottom_pixels, axis=0).reshape((2, 1))

        self.listener = tf.TransformListener()
        rospy.loginfo("Ready to start tf listener...")
        time.sleep(3)

        update_thread = threading.Thread(target=self.update)
        update_thread.daemon = True
        update_thread.start()

        show_thread = threading.Thread(target=self.show)
        show_thread.daemon = True
        show_thread.start()

    def update(self):
        while True:
            try:
                start_time = time.time()

                (trans, quaternion) = self.listener.lookupTransform('/map', '/base_link', rospy.Time(0))
                x = trans[0]
                y = trans[1]
                euler = self.euler_from_quaternion(quaternion)
                yaw = euler[2]
                self.robot_pose = (x, y, yaw)
                robot_pixel = self.coordinate_to_pixel(self.robot_pose)
                range_mask, obstacle_image, output_image = self.generate_image()
                parking_pixel = self.generate_parking(self.elevator_area_image, range_mask, obstacle_image,
                                                      output_image)
                if parking_pixel is None:
                    parking_pixel = self.generate_parking(self.waiting_area_image, range_mask, obstacle_image,
                                                          output_image)
                end_time = time.time()
                if parking_pixel is not None:
                    rospy.loginfo("Duration: %.4f. Parking coordinate (%.3f,%.3f)", end_time - start_time,
                                  *self.pixel_to_coordinate(parking_pixel))
                else:
                    rospy.loginfo("Duration: %.4f. no parking area", end_time - start_time)

                # draw the pixel of robot
                cv2.circle(output_image, (robot_pixel[1], robot_pixel[0]), self.robot_radius_pixels, self.ROBOT_COLOR,
                           -1)
                self.output_image = output_image
            except rospy.ROSException:
                self.shutdown()

    def distance_to_elevator_vector(self, pixel):
        pixel_vector = np.array(pixel).reshape((2, 1))
        return np.linalg.norm(self.elevator_bottom_vector - pixel_vector)

    def pixel_to_coordinate(self, row, col=None):
        """
        :type row: list|Tuple|int
        :type col: None|int
        """
        if hasattr(row, '__len__') and len(row) >= 2 and col is None:
            col = row[1]
            row = row[0]
        return [col * self.resolution + self.origin.x, (self.map_height - row) * self.resolution + self.origin.y]

    def coordinate_to_pixel(self, x, y=None):
        """
        :type x: list|Tuple|float
        :type y: None|float
        """
        if hasattr(x, '__len__') and len(x) >= 2 and y is None:
            y = x[1]
            x = x[0]
        return [self.map_height - int(round(((y - self.origin.y) / self.resolution))),
                int(round((x - self.origin.x) / self.resolution))]

    @staticmethod
    def euler_from_quaternion(quaternion):
        # noinspection PyUnresolvedReferences
        return tf.transformations.euler_from_quaternion(quaternion)  # type: list

    def get_laser_scan(self, detected_queue, occlusion_queue):
        (trans_laser, quaternion_laser) = self.listener.lookupTransform('/map', '/base_laser_link', rospy.Time(0))
        laser_message = rospy.wait_for_message("/scan", LaserScan, 1)  # type: LaserScan
        self.handle_scan(detected_queue, occlusion_queue, laser_message, trans_laser)

    def get_rgbd_scan(self, detected_queue, occlusion_queue):
        (trans_depth, quaternion_laser) = self.listener.lookupTransform('/map', '/rgbd_laser_link', rospy.Time(0))
        laser_message = rospy.wait_for_message("/rgbd_scan", LaserScan, 1)  # type: LaserScan
        self.handle_scan(detected_queue, occlusion_queue, laser_message, trans_depth)

    def handle_scan(self, detected_queue, occlusion_queue, laser_message, trans_base):
        detected_pixels = []
        distances = laser_message.ranges
        angle_min = laser_message.angle_min
        angle_increment = laser_message.angle_increment
        previous_pixel = (0, 0)

        for index in range(len(distances)):
            distance = distances[index]
            if math.isinf(distance) or math.isnan(distance):
                continue
            angle = angle_min + angle_increment * index + self.robot_pose[2]

            relative_x = np.cos(angle) * distance
            relative_y = np.sin(angle) * distance

            x = trans_base[0] + relative_x
            y = trans_base[1] + relative_y

            pixel = tuple(self.coordinate_to_pixel(x, y))
            if pixel == previous_pixel:
                continue
            else:
                previous_pixel = pixel
            item = (pixel, angle, distance)
            detected_pixels.append(item)
            detected_queue.put(item)

        self.get_occlusion_pixels(detected_pixels, trans_base, occlusion_queue)

    def get_occlusion_pixels(self, detected_pixels, trans_base, occlusion_queue):
        for pixel, angle, distance in detected_pixels:
            if (self.elevator_image[pixel[0], pixel[1]] == self.ELEVATOR_AREA_COLOR).all():
                times = 1
                while True:
                    expand_result, expand_pixel = self.laser_expand(angle, trans_base, distance, times)
                    if expand_result is False:
                        break
                    else:
                        occlusion_queue.put(expand_pixel)
                    times += 1

            elif (self.elevator_image[pixel[0], pixel[1]] == self.WAIT_AREA_COLOR).all():
                times = 1
                elevator_detected = False
                while True:
                    try:
                        expand_result, expand_pixel = self.laser_expand(angle, trans_base, distance, times)
                        if expand_result is False:
                            current_color = self.elevator_image[expand_pixel[0], expand_pixel[1]]
                            if elevator_detected is True or (current_color != self.WAIT_AREA_COLOR).any():
                                break
                        else:
                            elevator_detected = True
                            occlusion_queue.put(expand_pixel)
                    except IndexError:
                        break
                    times += 1

            occlusion_queue.put(pixel)

    def get_range_mask(self):
        mask_image = np.zeros((self.map_height, self.map_width, 1), np.uint8)

        radius = int(round(self.laser_radius / self.resolution))
        robot_pixel = self.coordinate_to_pixel(self.robot_pose)
        center = (robot_pixel[1], self.map_height - robot_pixel[0])
        axes = (radius, radius)

        angle = math.degrees(self.robot_pose[2] - self.laser_range / 2)
        start_angle = 0
        end_angle = math.degrees(self.laser_range)
        cv2.ellipse(mask_image, center, axes, angle, start_angle, end_angle, 255, -1)
        mask_image = cv2.flip(mask_image, 0)

        return mask_image, mask_image == 255

    def generate_image(self):
        obstacle_image = self.map_rgb.copy()

        detected_queue = Queue.Queue()
        occlusion_queue = Queue.Queue()

        laser_thread = threading.Thread(target=self.get_laser_scan, args=(detected_queue, occlusion_queue,))
        laser_thread.daemon = True
        laser_thread.start()

        rgbd_thread = threading.Thread(target=self.get_rgbd_scan, args=(detected_queue, occlusion_queue,))
        rgbd_thread.daemon = True
        rgbd_thread.start()

        laser_thread.join()
        rgbd_thread.join()
        del laser_thread
        del rgbd_thread

        mask_image = np.zeros((self.map_height, self.map_width, 1), np.uint8)

        while not occlusion_queue.empty():
            occlusion_pixel = occlusion_queue.get()
            cv2.circle(mask_image, (occlusion_pixel[1], occlusion_pixel[0]), 3, 255, -1)

        obstacle_image[
            np.logical_and((mask_image == 255).all(axis=2), self.obstacle_area_logical)] = self.OBSTACLE_COLOR

        output_image = obstacle_image.copy()
        range_mask, range_logical = self.get_range_mask()
        output_image[np.logical_and(range_logical, (output_image == (255, 255, 255)).all(axis=2))] = self.RANGE_COLOR

        while not detected_queue.empty():
            pixel, angle, distance = detected_queue.get()
            cv2.circle(output_image, (pixel[1], pixel[0]), 1, self.DETECTED_COLOR, -1)

        del detected_queue
        del occlusion_queue
        del mask_image

        return range_mask, obstacle_image, output_image

    def generate_parking(self, area_image, range_mask, obstacle_image, output_image):
        parking_radius_pixels = self.robot_radius_pixels + self.parking_extra_pixels

        obstacle_hsv_image = cv2.cvtColor(obstacle_image, cv2.COLOR_BGR2HSV)
        hsv_obstacle = self.rgb_to_hsv(self.OBSTACLE_COLOR)
        free_mask = 255 - cv2.inRange(obstacle_hsv_image, hsv_obstacle, hsv_obstacle)

        robot_mask = np.zeros((self.map_height, self.map_width, 1), np.uint8)
        robot_pixel = self.coordinate_to_pixel(self.robot_pose)
        cv2.circle(robot_mask, (robot_pixel[1], robot_pixel[0]),
                   int(round(parking_radius_pixels / (np.sqrt(2) / 2) + 1)), 255, -1)

        range_mask = cv2.bitwise_or(range_mask, robot_mask)
        area_image = cv2.bitwise_and(area_image, area_image, mask=range_mask)
        free_area_image = cv2.bitwise_and(area_image, area_image, mask=free_mask)
        gray_free_image = cv2.cvtColor(free_area_image, cv2.COLOR_BGR2GRAY)

        free_pixels = self.search_pixels(gray_free_image != 0)
        free_pixels.sort(key=self.distance_to_elevator_vector)

        for start_row, start_col in free_pixels:
            window = gray_free_image[
                     (start_row - parking_radius_pixels): (start_row + parking_radius_pixels + 1),
                     (start_col - parking_radius_pixels):(start_col + parking_radius_pixels + 1)
                     ]
            if len(np.where(window == 0)[0]) == 0:
                cv2.rectangle(output_image, (start_col - parking_radius_pixels, start_row - parking_radius_pixels),
                              (start_col + parking_radius_pixels, start_row + parking_radius_pixels),
                              self.PARKING_COLOR, -1)
                # cv2.circle(obstacle_image, (start_col, start_row), 4, (255, 0, 0), -1)
                return start_row, start_col
        return None

    @staticmethod
    def search_pixels(condition):
        pixels = []
        pixels_search = np.where(condition)
        for index in range(len(pixels_search[0])):
            pixels.append((pixels_search[0][index], pixels_search[1][index]))
        return pixels

    @staticmethod
    def rgb_to_hsv(rgb_color):
        rgb_image = np.uint8([[rgb_color]])
        hsv_image = cv2.cvtColor(rgb_image, cv2.COLOR_BGR2HSV)
        return hsv_image[0, 0]

    def show(self):
        while True:
            if self.output_image is not None:
                cv2.namedWindow("init1", cv2.WINDOW_NORMAL)
                cv2.imshow("init1", self.output_image)
                cv2.waitKey(50)
            else:
                time.sleep(1)

    def laser_expand(self, angle, start_trans, start_distance, times):
        relative_x = np.cos(angle) * (start_distance + times * self.resolution)
        relative_y = np.sin(angle) * (start_distance + times * self.resolution)

        x = start_trans[0] + relative_x
        y = start_trans[1] + relative_y

        pixel = self.coordinate_to_pixel(x, y)
        allowed_color = [self.ELEVATOR_AREA_COLOR, self.WAIT_AREA_COLOR]
        if tuple(self.elevator_image[pixel[0], pixel[1]]) not in allowed_color:
            return False, pixel
        return True, pixel


def main():
    ap = argparse.ArgumentParser()
    # noinspection PyTypeChecker
    ap.add_argument("--elevator", type=str, help="The path of elevator.png",
                    default=os.path.join(__DIR__, '../maps/init1/elevator.png'))
    args = vars(ap.parse_args())
    print("args", args)

    elevator_image_path = os.path.expandvars(args["elevator"])
    get_map = Scan(elevator_image_path)
    get_map.start()
    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()
