#!/usr/bin/env python
import rospy
import socket
import os
import time
import threading
import json
from speaker2 import Speaker


class Repeat:
    def __init__(self, tts_server, ip, port):
        self.ip = ip
        self.port = port
        self.tts_server = tts_server
        self.client = None  # type:socket.socket
        self.speaker = Speaker(self.tts_server)

    def start(self, ):
        rospy.loginfo("Try to connect STT server, IP: %s, Port: %d", self.ip, self.port)
        self.client = socket.socket()
        self.client.settimeout(30)
        try:
            self.client.connect((self.ip, self.port))
        except socket.error:
            rospy.logerr("STT Connected Failed")
            shutdown()
        rospy.loginfo("STT Connected successfully")

        heart_thread = threading.Thread(target=self._heart)
        heart_thread.daemon = True
        heart_thread.start()

        while True:
            try:
                message = self.client.makefile().readline().strip("\n").strip()
                if len(message) > 0:
                    rospy.loginfo("Get message: %s", message)
                    self.speaker.run(message)
            except socket.timeout:
                pass
            time.sleep(0.5)

    def _heart(self):
        while True:
            try:
                self.client.sendall("ping".encode())
            except socket.error:
                rospy.logerr("STT server is down")
                shutdown()
            time.sleep(3)


def shutdown():
    rospy.logwarn("Shutdown...")
    # noinspection PyProtectedMember
    os._exit(0)


def main():
    rospy.on_shutdown(shutdown)
    rospy.init_node('sc17cs_repeat', anonymous=True)
    params = {
        'tts_server': rospy.get_param('~tts_server', "http://127.0.0.1:1221"),
        'ip': rospy.get_param('~stt_ip', '127.0.0.1'),
        'port': rospy.get_param('-stt_port', 1201)
    }
    rospy.loginfo("Repeat params: %s", json.dumps(params))
    __repeat = Repeat(**params)
    __repeat.start()


if __name__ == '__main__':
    main()
