import json


class ParkingPose(object):
    ELEVATOR = 2
    WAITING = 1
    DEFAULT = 0

    def __init__(self, priority=-1, pose=None, pixel=None):
        if pose is None or pixel is None or priority == -1:
            self._priority = -1
            self._pose = None
            self._pixel = None
        else:
            self._priority = int(priority)
            if self._priority < -1 or self.priority > 2:
                raise ValueError("Invalid priority argument")
            self._pose = tuple([round(n, 2) for n in pose])
            if len(self._pose) != 3:
                raise ValueError("The pose must have 3 element (x, y, yaw).")
            self._pixel = tuple([int(n) for n in pixel])
            if len(self._pixel) != 2:
                raise ValueError("The pixel must have 2 element (col, row).")

    @property
    def priority(self):
        return self._priority

    @property
    def priority_trans(self):
        if self._priority == self.ELEVATOR:
            return "Elevator"
        elif self._priority == self.WAITING:
            return "Waiting"
        elif self._priority == self.DEFAULT:
            return "Default"
        else:
            return "None"

    @property
    def pose(self):
        return self._pose

    @property
    def pixel(self):
        return self._pixel

    def json(self):
        return json.dumps({"priority": self._priority, "pose": self._pose, "pixel": self._pixel})

    @staticmethod
    def load(json_string):
        data = json.loads(json_string)
        if "priority" in data and "pose" in data and "pixel" in data:
            return ParkingPose(data["priority"], data["pose"], data["pixel"])
        return ParkingPose()

    def __str__(self):
        result = "Priority: %d (%s), " % (self._priority, self.priority_trans)
        if self._pose is None:
            result += "Pose: None, "
        else:
            result += "Pose(%.3f, %.3f,%.3f), " % (self._pose[0], self._pose[1], self._pose[2])

        if self._pixel is None:
            result += "Pixel: None"
        else:
            result += "Pixel(%d, %d)" % (self._pixel[0], self._pixel[1])

        return result
