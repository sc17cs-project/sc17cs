import math
import rospy
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import actionlib
from geometry_msgs.msg import Pose, Point, Quaternion
import tf
from actionlib_msgs.msg import *


class BaseController:
    def __init__(self):
        self.goal_sent = False
        self.client = actionlib.SimpleActionClient("move_base", MoveBaseAction)

        # Allow up to 5 seconds for the action server to come up
        self.client.wait_for_server(rospy.Duration(5))
        rospy.loginfo("Base server ready")

    def goto(self, position, radians=0, degrees=None, wait=60):
        """
        Synchronization method
        :param position:
        :param radians:
        :param degrees:
        :param wait:
        :return:
        """
        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = 'map'
        goal.target_pose.header.stamp = rospy.Time.now()
        if degrees is not None:
            theta = math.radians(degrees)
        else:
            theta = radians
        # noinspection PyUnresolvedReferences
        quaternion = tf.transformations.quaternion_from_euler(0, 0, theta)

        quaternion = {'r1': 0.000, 'r2': 0.000, 'r3': quaternion[2], 'r4': quaternion[3]}

        goal.target_pose.pose = Pose(Point(position[0], position[1], 0.000),
                                     Quaternion(quaternion['r1'], quaternion['r2'], quaternion['r3'], quaternion['r4']))

        rospy.loginfo("Go to (%.3f, %.3f, %.3f) pose", position[0], position[1], theta)

        # Start moving
        self.goal_sent = True
        self.client.send_goal(goal)

        # Allow TurtleBot up to 60 seconds to complete task
        done = self.client.wait_for_result(rospy.Duration(wait))
        self.goal_sent = False

        state = self.client.get_state()
        if done and state == GoalStatus.SUCCEEDED:
            return True
        return state

    def cancel(self):
        """
        Cancel goal
        :return:
        """
        if self.goal_sent is True:
            state = self.client.get_state()
            if state == GoalStatus.PENDING or state == GoalStatus.ACTIVE:
                self.client.cancel_goal()
                self.goal_sent = False


if __name__ == '__main__':
    def main():
        rospy.init_node('base_test', anonymous=True)
        action = BaseController()
        state = action.goto((6.28, -1.83), degrees=90)
        print (state)


    main()
