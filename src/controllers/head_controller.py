#!/usr/bin/env python

import rospy
import actionlib
from control_msgs.msg import FollowJointTrajectoryAction, FollowJointTrajectoryGoal
from trajectory_msgs.msg import JointTrajectoryPoint


class HeadController:
    def __init__(self):
        self.client = actionlib.SimpleActionClient("head_controller/follow_joint_trajectory",
                                                   FollowJointTrajectoryAction)
        self.client.wait_for_server(rospy.Duration(5))
        rospy.loginfo("Head server ready")

    def goto(self, joint1, joint2, time_from_start=1, wait=3):
        """
        Synchronization method
        :param joint1:
        :param joint2:
        :param time_from_start:
        :param wait:
        :return:
        """
        goal = FollowJointTrajectoryGoal()
        goal.trajectory.joint_names = ["head_1_joint", "head_2_joint"]
        point = JointTrajectoryPoint()
        point.positions = [joint1, joint2]
        point.time_from_start = rospy.Duration(time_from_start)
        goal.trajectory.points.append(point)

        self.client.send_goal(goal)
        done = self.client.wait_for_result(rospy.Duration(wait))
        state = self.client.get_state()
        if done and state == actionlib.GoalStatus.SUCCEEDED:
            return True
        return state


if __name__ == '__main__':
    def main():
        rospy.init_node('head_test', anonymous=True)
        action = HeadController()
        state = action.goto(0.0, -0.20)
        print (state)


    main()
