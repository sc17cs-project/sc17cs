#!/usr/bin/env python

from numpy import ndarray
import rospy
import cv2
from sensor_msgs.msg import Image
from sensor_msgs.msg import CompressedImage
from cv_bridge import CvBridge
import threading
import time
import os
import numpy as np


class Show:
    def __init__(self):
        self.output_image = None  # type: ndarray
        self.elevator_image = None  # type: ndarray
        self.current_camera = None  # type: ndarray
        self.map_image = None  # type: ndarray
        self.bridge = CvBridge()
        rospy.Subscriber("sc17cs/move/output", Image, self._make_output)
        rospy.Subscriber("sc17cs/scan/output/elevator", Image, self._make_elevator)
        # rospy.Subscriber("sc17cs/scan/output/map", Image, self._make_map)
        rospy.Subscriber("/xtion/rgb/image_raw/compressed", CompressedImage, self._camera_callback)

    def start(self):
        show_thread = threading.Thread(target=self._show)
        show_thread.daemon = True
        show_thread.start()

    def _make_output(self, output_data):
        self.output_image = self.bridge.imgmsg_to_cv2(output_data, "bgr8")

    def _make_elevator(self, output_data):
        self.elevator_image = self.bridge.imgmsg_to_cv2(output_data, "bgr8")

    def _make_map(self, output_data):
        self.map_image = self.bridge.imgmsg_to_cv2(output_data, "bgr8")

    def _camera_callback(self, data):
        np_arr = np.fromstring(data.data, np.uint8)
        image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
        self.current_camera = image_np

    def _show(self):
        while not rospy.is_shutdown():
            if self.output_image is not None:
                cv2.namedWindow("output", cv2.WINDOW_NORMAL)
                cv2.imshow("output", self.output_image)
            if self.elevator_image is not None:
                cv2.namedWindow("elevator", cv2.WINDOW_NORMAL)
                cv2.imshow("elevator", self.elevator_image)
            if self.map_image is not None:
                cv2.namedWindow("map", cv2.WINDOW_NORMAL)
                cv2.imshow("map", self.map_image)
            if self.current_camera is not None:
                # cv2.namedWindow("camera", cv2.WINDOW_NORMAL)
                cv2.imshow("camera", self.current_camera)
            cv2.waitKey(50)


def shutdown():
    rospy.logwarn("Shutdown...")
    cv2.destroyAllWindows()
    # noinspection PyProtectedMember
    os._exit(0)


def main():
    rospy.on_shutdown(shutdown)
    rospy.init_node('sc17cs_show')

    __show = Show()
    __show.start()

    # response the ctrl+c
    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()
